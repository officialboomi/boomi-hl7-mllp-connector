package com.boomi.connector.mllp;

import java.util.Arrays;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;

import com.boomi.connector.api.listen.Listener;
import com.boomi.util.LogUtil;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;

public class SimpleTCPChannelInitializer extends ChannelInitializer<SocketChannel> {

//	private static final String PROTOCOL = "TLSv1.2";
	private static final Logger logger = LogUtil.getLogger(SimpleTCPChannelInitializer.class);
	Listener _listener;
	String _MLLPWrapper;
	KeyManagerFactory _kmf;
	TrustManagerFactory _tmf;
	int _maxFileSize;
	boolean _functionalAck;
	String _desiredCipherSuite;
	String _nonHL7Response;
	String _nonHL7ResponseError;
	int _port;
	String _isEDIX12;

	public SimpleTCPChannelInitializer(Listener listener, String MLLPWrapper, KeyManagerFactory kmf,
									   TrustManagerFactory tmf, int maxFileSize, boolean functionalAck, String desiredCipherSuite, String nonHL7Response, String nonHL7ResponseError, int port, String isEDIX12) {
		_listener = listener;
		_MLLPWrapper = MLLPWrapper;
		_kmf = kmf;
		_tmf = tmf;
		_maxFileSize = maxFileSize;
		_functionalAck = functionalAck;
		_desiredCipherSuite = desiredCipherSuite;
		_nonHL7Response = nonHL7Response;
		_nonHL7ResponseError = nonHL7ResponseError;
		_port = port;
		_isEDIX12 = isEDIX12;
	}

	protected void initChannel(SocketChannel socketChannel) throws Exception {

		SslHandler sslHandler;
		SSLEngine engine;
		SslContext serverContext;
		
		if (_kmf != null && _tmf != null) {
			logger.info("2 way SSL ");

			if (!_desiredCipherSuite.isEmpty()) {
				String[] CIPHER_SUITES = { _desiredCipherSuite };
				serverContext = SslContextBuilder.forServer(_kmf).trustManager(_tmf).protocols("TLSv1.3", "TLSv1.2")
						.ciphers(Arrays.asList(CIPHER_SUITES)).build();
			} else {
				serverContext = SslContextBuilder.forServer(_kmf).trustManager(_tmf).protocols("TLSv1.3", "TLSv1.2")
						.build();
			}
			engine = serverContext.newEngine(socketChannel.alloc());
			engine.setNeedClientAuth(true);

			sslHandler = new SslHandler(engine);
			socketChannel.pipeline().addFirst("ssl", sslHandler);
		} else if (_kmf != null) {
			logger.info("1 way SSL ");

			if (!_desiredCipherSuite.isEmpty()) {
				String[] CIPHER_SUITES = { _desiredCipherSuite };
				serverContext = SslContextBuilder.forServer(_kmf).protocols("TLSv1.3", "TLSv1.2")
						.ciphers(Arrays.asList(CIPHER_SUITES)).build();
			} else {
				serverContext = SslContextBuilder.forServer(_kmf).protocols("TLSv1.3", "TLSv1.2").build();
			}

			engine = serverContext.newEngine(socketChannel.alloc());
			engine.setUseClientMode(false);
			sslHandler = new SslHandler(engine);
			socketChannel.pipeline().addLast(sslHandler);

		}
		

		if (_MLLPWrapper.equals("Yes")) {
			socketChannel.pipeline().addLast("frameDecoder", new DelimiterBasedFrameDecoder(100000000, // Maximum frame
																										// length
					Unpooled.wrappedBuffer(new byte[] { 0x0B }), // MLLP start marker
					Unpooled.wrappedBuffer(new byte[] { 0x1C }) // MLLP end marker
			));
		}
		socketChannel.pipeline().addLast(new StringEncoder());
		socketChannel.pipeline().addLast(new StringDecoder());
		socketChannel.pipeline()
				.addLast(new SimpleTCPChannelHandler(_listener, _MLLPWrapper, _maxFileSize, _functionalAck, _kmf, _nonHL7Response, _nonHL7ResponseError, _port, _isEDIX12));
		// int _maxFileSize;

		// socketChannel.pipeline().addLast(new SimpleTCPChannelHandlerHL7(_listener,
		// _MLLPWrapper, null, null));
	}

}
