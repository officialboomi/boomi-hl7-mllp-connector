package com.boomi.connector.mllp;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.listen.Listener;
import com.boomi.util.LogUtil;

import ca.uhn.hl7v2.HapiContext;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

class SimpleNettyServerBootstrapHL7 implements Runnable {

    private static final Logger logger = LogUtil.getLogger(SimpleNettyServerBootstrapHL7.class);

    int _port;
    Listener _listener;
    //	String _MLLPWrapper;
    ServerBootstrap b = new ServerBootstrap();
    EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    EventLoopGroup workerGroup = new NioEventLoopGroup();
    ChannelFuture f = null;
    KeyManagerFactory _kmf;
    TrustManagerFactory _tmf;
    HapiContext _context;
    OperationContext _operationContext;
    boolean _autoAcknowledge;
    int _maxFileSize;
    boolean _msgAffinity;
    String _validationLevel;
    String _MSH3;
    String _MSH4;
    String _MSH5;
    String _MSH6;
    String _MSH12;
    String _hL7Delimiter;
    String _desiredCipherSuite;
    private boolean success;
    boolean _useSecureConnection;

    public boolean isSuccess() {
        return success;
    }

    public SimpleNettyServerBootstrapHL7(int port, Listener listener, KeyManagerFactory kmf, TrustManagerFactory tmf,
                                         int maxFileSize, OperationContext operationContext, HapiContext context, boolean autoAcknowledge,
                                         boolean msgAffinity, String validationLevel, String mSH3, String mSH4, String mSH5, String mSH6,
                                         String mSH12, String hL7Delimiter, String desiredCipherSuite, boolean useSecureConnection) {
        _port = port;
        // _MLLPWrapper = MLLPWrapper;
        _kmf = kmf;
        _tmf = tmf;
        _context = context;
        _operationContext = operationContext;
        _listener = listener;
        _autoAcknowledge = autoAcknowledge;
        _maxFileSize = maxFileSize;
        _msgAffinity = msgAffinity;
        _validationLevel = validationLevel;
        _MSH3 = mSH3;
        _MSH4 = mSH4;
        _MSH5 = mSH5;
        _MSH6 = mSH6;
        _MSH12 = mSH12;
        _hL7Delimiter = hL7Delimiter;
        _desiredCipherSuite = desiredCipherSuite;
        _useSecureConnection = useSecureConnection;
    }

    @Override
    public void run() {

        try {

            b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.DEBUG))
                    .childHandler(new SimpleTCPChannelInitializerHL7(_listener, _kmf, _tmf, _maxFileSize,
                            _operationContext, _context, _autoAcknowledge, _msgAffinity, _validationLevel, _MSH3, _MSH4,
                            _MSH5, _MSH6, _MSH12, _hL7Delimiter, _desiredCipherSuite, _useSecureConnection, _port))
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
//					.handler(new ExceptionHandler());

            // Bind and start to accept incoming connections.

            logger.info("Starting server at: " + _port);

            f = b.bind(_port).sync();
            f.addListener((ChannelFutureListener) bindFuture -> {
                success = bindFuture.isSuccess();
                // Notify the calling thread about the result
                synchronized (SimpleNettyServerBootstrapHL7.this) {
                    SimpleNettyServerBootstrapHL7.this.notify();
                }
            });

            f.addListener((ChannelFutureListener) future1 -> {
                if (future1.isSuccess()) {
                    logger.info(" Server started --- successfully");
                } else {
                    // Server failed to start
                    logger.info(" Server start --- failure");
                    //                  Throwable cause = future1.cause();
                }
            });

            // f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            success = false;
            throw new ConnectorException("Connector not active, check logs for more information" + e);
            // TODO Auto-generated catch block
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new ConnectorException("Connector not active, check logs for more information");
        }

    }

    public void close() {
        logger.warning("Server stop requested");
        _listener = null;
        if (f.channel() != null && f.channel().isActive()) {
		// Gracefully close the Channel
            f.channel().close().addListener((ChannelFutureListener) future -> {
                if (future.isSuccess()) {
                    logger.info("Channel closed successfully.");
                } else {
                    logger.severe("Failed to close the channel: " + future.cause());
                }
            });
        }
        // Shutdown the boss groups
        bossGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
        //  wait for the boss groups to be terminated
        try {
            bossGroup.terminationFuture().sync();
            workerGroup.terminationFuture().sync();
            logger.info("Boss groups have been terminated successfully.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
