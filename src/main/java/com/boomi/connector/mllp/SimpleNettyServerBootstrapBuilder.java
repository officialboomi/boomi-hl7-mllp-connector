package com.boomi.connector.mllp;

import com.boomi.connector.api.listen.Listener;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

public class SimpleNettyServerBootstrapBuilder {
    private int port;
    private Listener listener;
    private String mllpWrapper;
    private KeyManagerFactory kmf;
    private TrustManagerFactory tmf;
    private int maxFileSize;
    private boolean functionalAck;
    private String desiredCipherSuite;
    private String nonHL7Response;
    private String nonHL7ResponseError;
    private String isEDIX12;

    public SimpleNettyServerBootstrapBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setMLLPWrapper(String mllpWrapper) {
        this.mllpWrapper = mllpWrapper;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setKmf(KeyManagerFactory kmf) {
        this.kmf = kmf;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setTmf(TrustManagerFactory tmf) {
        this.tmf = tmf;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setMaxFileSize(int maxFileSize) {
        this.maxFileSize = maxFileSize;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setFunctionalAck(boolean functionalAck) {
        this.functionalAck = functionalAck;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setDesiredCipherSuite(String desiredCipherSuite) {
        this.desiredCipherSuite = desiredCipherSuite;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setNonHL7Response(String nonHL7Response) {
        this.nonHL7Response = nonHL7Response;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setNonHL7ResponseError(String nonHL7ResponseError) {
        this.nonHL7ResponseError = nonHL7ResponseError;
        return this;
    }

    public SimpleNettyServerBootstrapBuilder setIsEDIX12(String isEDIX12) {
        this.isEDIX12 = isEDIX12;
        return this;
    }

    public SimpleNettyServerBootstrap createSimpleNettyServerBootstrap() {
        return new SimpleNettyServerBootstrap(port, listener, mllpWrapper, kmf, tmf, maxFileSize, functionalAck, desiredCipherSuite, nonHL7Response, nonHL7ResponseError, isEDIX12);
    }
}