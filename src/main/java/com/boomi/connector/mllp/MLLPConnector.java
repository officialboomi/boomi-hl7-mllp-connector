package com.boomi.connector.mllp;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.listen.UnmanagedListenConnector;
import com.boomi.connector.util.listen.UnmanagedListenOperation;

public class MLLPConnector extends UnmanagedListenConnector{//BaseListenConnector {
   
    private MLLPConnection createConnection(BrowseContext context) {
        return new MLLPConnection(context);
    }
	
	@Override
	public UnmanagedListenOperation createListenOperation(OperationContext context) {
		return new MLLPUnmanagedListenOperation(context);
	}

	@Override
	public Browser createBrowser(BrowseContext context) {
		// TODO Auto-generated method stub
		return null;
	}
}