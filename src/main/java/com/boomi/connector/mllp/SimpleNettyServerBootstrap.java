package com.boomi.connector.mllp;

import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import com.boomi.connector.api.listen.Listener;
import com.boomi.util.LogUtil;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

class SimpleNettyServerBootstrap implements Runnable {

	private static final Logger logger = LogUtil.getLogger(SimpleNettyServerBootstrap.class);

	int _port;
	Listener _listener;
	String _MLLPWrapper;
	ServerBootstrap b = new ServerBootstrap();
	EventLoopGroup bossGroup = new NioEventLoopGroup();
	EventLoopGroup workerGroup = new NioEventLoopGroup();
	ChannelFuture f;
	KeyManagerFactory _kmf;
	TrustManagerFactory _tmf;
	int _maxFileSize;
	boolean _functionalAck;
	String _desiredCipherSuite;
	String _nonHL7Response, _nonHL7ResponseError, _isEDIX12;
	private boolean success;

	public boolean isSuccess() {
		return success;
	}

	public SimpleNettyServerBootstrap(int port, Listener listener, String MLLPWrapper, KeyManagerFactory kmf,
									  TrustManagerFactory tmf, int maxFileSize, boolean functionalAck, String desiredCipherSuite,
									  String nonHL7Response, String nonHL7ResponseError, String isEDIX12) {
		_port = port;
		_listener = listener;
		_MLLPWrapper = MLLPWrapper;
		_kmf = kmf;
		_tmf = tmf;
		_maxFileSize = maxFileSize;
		_functionalAck = functionalAck;
		_desiredCipherSuite = desiredCipherSuite;
		_nonHL7Response = nonHL7Response;
		_nonHL7ResponseError = nonHL7ResponseError;
		_isEDIX12 = isEDIX12;
	}

	@Override
	public void run() {

		try {
			b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.childHandler(new SimpleTCPChannelInitializer(_listener, _MLLPWrapper, _kmf, _tmf, _maxFileSize,
							_functionalAck, _desiredCipherSuite, _nonHL7Response, _nonHL7ResponseError, _port, _isEDIX12))
					.childOption(ChannelOption.SO_KEEPALIVE, true);

			logger.info("Starting server at: " + _port);
			f = b.bind(_port).sync();
			f.addListener((ChannelFutureListener) bindFuture -> {
				success = bindFuture.isSuccess();
				// Notify the calling thread about the result
				synchronized (SimpleNettyServerBootstrap.this) {
					SimpleNettyServerBootstrap.this.notify();
				}
			});

			f.addListener((ChannelFutureListener) future1 -> {
				if (future1.isSuccess()) {
					logger.info(" Server started --- successfully");
				} else {
					logger.info(" Server start --- failure");
				}
			});

		} catch (InterruptedException e) {
			logger.severe("Error starting listener on port " + _port);
			e.printStackTrace();
		}
	}

	public void close() {
		logger.warning("Server stop requested");
		_listener = null;
		if (f.channel() != null && f.channel().isActive()) {
			f.channel().close().addListener((ChannelFutureListener) future -> {
				if (future.isSuccess()) {
					logger.info("Channel closed successfully.");
				} else {
					logger.severe("Failed to close the channel: " + future.cause());
				}
			});
		}
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		try {
			bossGroup.terminationFuture().sync();
			workerGroup.terminationFuture().sync();
			logger.info("Boss groups have been terminated successfully.");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
