package com.boomi.connector.mllp;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.listen.Listener;
import com.boomi.util.LogUtil;

import ca.uhn.hl7v2.HapiContext;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslHandler;

public class SimpleTCPChannelInitializerHL7 extends ChannelInitializer<SocketChannel> {

	private static final Logger logger = LogUtil.getLogger(SimpleTCPChannelInitializerHL7.class);
	Listener _listener;
//	String _MLLPWrapper;
	KeyManagerFactory _kmf;
	TrustManagerFactory _tmf;
	OperationContext _operationContext;
	HapiContext _context;
	boolean _autoAcknowledge;
	int _maxFileSize;
	boolean _msgAffinity;
	String _validationLevel;
	String _MSH3;
	String _MSH4;
	String _MSH5;
	String _MSH6;
	String _MSH12;
	String _hL7Delimiter;
	String _desiredCipherSuite;
	boolean _useSecureConnection;
	int _port;

	public SimpleTCPChannelInitializerHL7(Listener listener, KeyManagerFactory kmf, TrustManagerFactory tmf,
			int maxFileSize, OperationContext operationContext, HapiContext context, boolean autoAcknowledge,
			boolean msgAffinity, String validationLevel, String mSH3, String mSH4, String mSH5, String mSH6,
			String mSH12, String hL7Delimiter, String desiredCipherSuite,boolean useSecureConnection, int port) {
		_listener = listener;
//		_MLLPWrapper = MLLPWrapper;
		_kmf = kmf;
		_tmf = tmf;
		_operationContext = operationContext;
		_context = context;
		_autoAcknowledge = autoAcknowledge;
		_maxFileSize = maxFileSize;
		_msgAffinity = msgAffinity;
		_validationLevel = validationLevel;
		_MSH3 = mSH3;
		_MSH4 = mSH4;
		_MSH5 = mSH5;
		_MSH6 = mSH6;
		_MSH12 = mSH12;
		_hL7Delimiter = hL7Delimiter;
		_desiredCipherSuite = desiredCipherSuite;
		_useSecureConnection = useSecureConnection;
		_port = port;
	}

	protected void initChannel(SocketChannel socketChannel) throws IOException, InvalidKeySpecException,
			NoSuchAlgorithmException, CertificateException {

		SslHandler sslHandler;
		SSLEngine engine;
		SslContext serverContext;

		// if (!_desiredCipherSuite.isEmpty()) {
		// SSLParameters sslParameters =
		// SSLContext.getDefault().getDefaultSSLParameters();

		// Set the desired cipher suite in the list of enabled cipher suites
		// List<String> enabledCipherSuites =
		// Arrays.asList(sslParameters.getCipherSuites());
		// enabledCipherSuites.retainAll(Arrays.asList(_desiredCipherSuite)); // Keep
		// only the desired cipher suite
		// sslParameters.setCipherSuites(enabledCipherSuites.toArray(new String[0]));
		// serverContext.getDefaultSSLParameters().setCipherSuites(sslParameters.getCipherSuites());
		// }

		if (_kmf != null && _tmf != null) {
			if (!_desiredCipherSuite.isEmpty()) {
				String[] CIPHER_SUITES = { _desiredCipherSuite };
				serverContext = SslContextBuilder.forServer(_kmf).trustManager(_tmf).protocols("TLSv1.3", "TLSv1.2")
						.ciphers(Arrays.asList(CIPHER_SUITES)).build();
			} else {
				serverContext = SslContextBuilder.forServer(_kmf).trustManager(_tmf).protocols("TLSv1.3", "TLSv1.2")
						.build();
			}
			engine = serverContext.newEngine(socketChannel.alloc());
			engine.setNeedClientAuth(true);

			sslHandler = new SslHandler(engine);
			socketChannel.pipeline().addFirst("ssl", sslHandler);
		} else if (_kmf != null) {
			if (!_desiredCipherSuite.isEmpty()) {
				String[] CIPHER_SUITES = { _desiredCipherSuite };
				serverContext = SslContextBuilder.forServer(_kmf).protocols("TLSv1.3", "TLSv1.2")
						.ciphers(Arrays.asList(CIPHER_SUITES)).build();
			} else {
				serverContext = SslContextBuilder.forServer(_kmf).protocols("TLSv1.3", "TLSv1.2").build();
			}

			engine = serverContext.newEngine(socketChannel.alloc());
			engine.setUseClientMode(false);
			sslHandler = new SslHandler(engine);
			socketChannel.pipeline().addLast(sslHandler);

		}

		socketChannel.pipeline().addLast("frameDecoder", new DelimiterBasedFrameDecoder(100000000, // Maximum frame
																									// length
				Unpooled.wrappedBuffer(new byte[] { 0x0B }), // MLLP start marker
				Unpooled.wrappedBuffer(new byte[] { 0x1C }) // MLLP end marker
		));
		socketChannel.pipeline().addLast(new StringEncoder());
		socketChannel.pipeline().addLast(new StringDecoder());
		socketChannel.pipeline()
				.addLast(new SimpleTCPChannelHandlerHL7(_listener, _operationContext, _context, _autoAcknowledge,
						_maxFileSize, _msgAffinity, _validationLevel, _MSH3, _MSH4, _MSH5, _MSH6, _MSH12, _hL7Delimiter,
						_kmf, _useSecureConnection, _port));

	}
}
