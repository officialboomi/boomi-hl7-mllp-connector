package com.boomi.connector.mllp;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.validation.impl.ValidationContextFactory;
import com.boomi.connector.api.*;
import com.boomi.connector.api.listen.ListenManager;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.listen.SingletonListenOperation;
import com.boomi.connector.util.listen.UnmanagedListenOperation;
import com.boomi.util.LogUtil;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;
import java.beans.ExceptionListener;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.logging.Logger;

public class MLLPUnmanagedListenOperation extends UnmanagedListenOperation
        implements SingletonListenOperation<ListenManager>, ExceptionListener {

    private static final Logger logger = LogUtil.getLogger(MLLPUnmanagedListenOperation.class);
    //static final Logger responseLogger = LogUtil.getLogger(OperationResponse.class);
    public static String workDir;

    Listener _listener;
    PropertyMap connProps;

    boolean nonHL7;
    SimpleNettyServerBootstrap simpleNettyServerBootstrap;
    SimpleNettyServerBootstrapHL7 simpleNettyServerBootstrapHL7;

    /**
     * Creates a new instance using the provided operation context
     */
    protected MLLPUnmanagedListenOperation(OperationContext context) {
        super(context);
        logger.info("Constructing MLLPUnmanagedListenOperation");
    }

    @Override
    protected void start(Listener listener) {
        logger.info("MLLPUnmanagedListenOperation.start");
        _listener = listener;
        HapiContext context = new DefaultHapiContext();
        // Initialize all the connection properties
        connProps = getContext().getConnectionProperties();
        int port = connProps.getLongProperty("portNumber").intValue();
        int messageSize = connProps.getLongProperty("messageSize").intValue();
        String desiredCipherSuite = connProps.getProperty("setCipherSuite", "");
        // Convert maximum message size from KB to Bytes
        int maxFileSize = messageSize * 1000;
        logger.info("maxFileSize:" + maxFileSize);
        String validationLevel = connProps.getProperty("validationLevel", "1");
        boolean useSecureConnection = connProps.getBooleanProperty("useSecureConnection", false);
        boolean functionalAck = connProps.getBooleanProperty("FunctionalAck", true);
        boolean autoAcknowledge = connProps.getBooleanProperty("autoAcknowledge", true);
        String MSH3 = connProps.getProperty("MSH3", "");
        String MSH4 = connProps.getProperty("MSH4", "");
        String MSH5 = connProps.getProperty("MSH5", "");
        String MSH6 = connProps.getProperty("MSH6", "");
        String MSH12 = connProps.getProperty("MSH12", "");
        String HL7Delimiter = connProps.getProperty("HL7Delimiter", "|");

        nonHL7 = connProps.getBooleanProperty("nonHL7", false);
        String isEDIX12 = connProps.getProperty("EDIX12", "Yes");
        String MLLPWrapper = connProps.getProperty("nonHL7MLLPWrapper", "No");
        String nonHL7Response = connProps.getProperty("nonHL7Response", "OK");
        String nonHL7ResponseError = connProps.getProperty("nonHL7ResponseError", "ERROR");

        PrivateKeyStore serverPrivateSSLKeyStore;
        serverPrivateSSLKeyStore = connProps.getPrivateKeyStoreProperty("serverPrivateSSLKey");
        String serverPrivateSSLKeyPassword;
        PublicKeyStore partnerPublicSSLKeyStore = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate");
        PublicKeyStore partnerPublicSSLKeyStore2 = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate2");
        PublicKeyStore partnerPublicSSLKeyStore3 = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate3");
        PublicKeyStore partnerPublicSSLKeyStore4 = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate4");
        PublicKeyStore partnerPublicSSLKeyStore5 = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate5");
        PublicKeyStore partnerPublicSSLKeyStore6 = connProps.getPublicKeyStoreProperty("partnerPublicSSLCertificate6");


        workDir = connProps.getProperty("workDir");
        // Initialize all the connection properties

        KeyManagerFactory kmf = null;
        TrustManagerFactory tmf = null;


        // Non-TLS HL7 and non-HL7 server initiator start
        if (!useSecureConnection) {
            if (!nonHL7) {
                logger.info("Starting SimpleThreaded TLS server - for HL7 on port " + port);
                context.setValidationContext(ValidationContextFactory.noValidation());
                simpleNettyServerBootstrapHL7 = new SimpleNettyServerBootstrapHL7(port, _listener, null, null,
                        maxFileSize, this.getContext(), context, autoAcknowledge, functionalAck, validationLevel, MSH3,
                        MSH4, MSH5, MSH6, MSH12, HL7Delimiter, desiredCipherSuite, false);
                Thread t = new Thread(simpleNettyServerBootstrapHL7);
                t.start();
            } else {
                logger.info("Starting SimpleThreaded TLS server - for non HL7 on port " + port);
                simpleNettyServerBootstrap = new SimpleNettyServerBootstrapBuilder().setPort(port).setListener(_listener).setMLLPWrapper(MLLPWrapper).setKmf(null).setTmf(null).setMaxFileSize(maxFileSize).setFunctionalAck(functionalAck).setDesiredCipherSuite(desiredCipherSuite).setNonHL7Response(nonHL7Response).setNonHL7ResponseError(nonHL7ResponseError).setIsEDIX12(isEDIX12).createSimpleNettyServerBootstrap();
                Thread t = new Thread(simpleNettyServerBootstrap);
                t.start();
//				synchronized (simpleNettyServerBootstrap) {
//					try {
//						simpleNettyServerBootstrap.wait();
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						// throw new ConnectorException(e);
//					}
//				}
//
//				boolean success = simpleNettyServerBootstrap.isSuccess();
//				if (success) {
//					logger.info("Server started successfully");
//				} else {
//					logger.severe("Server start failed on port" + port);
//					throw new ConnectorException("Server start failed on port " + port);
//					// Handle the failure condition
//				}
            }
        }

        // Non-TLS HL7 and non-HL7 server initiator ends

        // TLS HL7 and non-HL7 server initiator start

        if (useSecureConnection) {
            KeyStore serverKeyStore;
            char[] pwd;
            if (serverPrivateSSLKeyStore != null && serverPrivateSSLKeyStore.getKeyStore() != null) {
                serverPrivateSSLKeyPassword = serverPrivateSSLKeyStore.getPassword();
                serverKeyStore = serverPrivateSSLKeyStore.getKeyStore();
                pwd = serverPrivateSSLKeyPassword.toCharArray();
                try {
                    kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                    kmf.init(serverKeyStore, pwd);
                    if (partnerPublicSSLKeyStore != null) {
                        try {

                            if (partnerPublicSSLKeyStore.getKeyStore() != null) {
                                tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                                tmf.init(partnerPublicSSLKeyStore.getKeyStore());
                                if (partnerPublicSSLKeyStore2 != null && partnerPublicSSLKeyStore2.getKeyStore() != null) {
                                        tmf.init(partnerPublicSSLKeyStore2.getKeyStore());
                                    }
                                if (partnerPublicSSLKeyStore3 != null) {
                                    if (partnerPublicSSLKeyStore3.getKeyStore() != null)
                                        tmf.init(partnerPublicSSLKeyStore3.getKeyStore());
                                }
                                if (partnerPublicSSLKeyStore4 != null) {
                                    if (partnerPublicSSLKeyStore4.getKeyStore() != null)
                                        tmf.init(partnerPublicSSLKeyStore4.getKeyStore());
                                }
                                if (partnerPublicSSLKeyStore5 != null) {
                                    if (partnerPublicSSLKeyStore5.getKeyStore() != null)
                                        tmf.init(partnerPublicSSLKeyStore5.getKeyStore());
                                }
                                if (partnerPublicSSLKeyStore6 != null) {
                                    if (partnerPublicSSLKeyStore6.getKeyStore() != null)
                                        tmf.init(partnerPublicSSLKeyStore6.getKeyStore());
                                }
                            }
                        } catch (KeyStoreException e) {
                            logger.severe("trust ManagerFactory error: check the certificates");
                            e.printStackTrace();
                        }
                    }
                } catch (UnrecoverableKeyException e) {
                    // TODO Auto-generated catch block
                    logger.severe("UnrecoverableKeyException error: check the certificate");
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    logger.severe("KeyManagerFactory error: check the certificate");
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    logger.severe("NoSuchAlgorithmException error: check the certificate");
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (!nonHL7) {
                logger.info("Starting SimpleThreaded tcp TLS server - for HL7 on port " + port);
                context.setValidationContext(ValidationContextFactory.noValidation());
                simpleNettyServerBootstrapHL7 = new SimpleNettyServerBootstrapHL7(port, _listener, kmf, tmf,
                        maxFileSize, this.getContext(), context, autoAcknowledge, functionalAck, validationLevel, MSH3,
                        MSH4, MSH5, MSH6, MSH12, HL7Delimiter, desiredCipherSuite, true);
                Thread t = new Thread(simpleNettyServerBootstrapHL7);
                t.start();
            } else {
                logger.info("Starting SimpleThreaded tcp TLS server - for non HL7 on port " + port);
                simpleNettyServerBootstrap = new SimpleNettyServerBootstrapBuilder().setPort(port).setListener(_listener).setMLLPWrapper(MLLPWrapper).setKmf(kmf).setTmf(tmf).setMaxFileSize(maxFileSize).setFunctionalAck(functionalAck).setDesiredCipherSuite(desiredCipherSuite).setNonHL7Response(nonHL7Response).setNonHL7ResponseError(nonHL7ResponseError).setIsEDIX12(isEDIX12).createSimpleNettyServerBootstrap();
                Thread t = new Thread(simpleNettyServerBootstrap);
                t.start();
            }
        }

        // TLS HL7 and non-HL7 server initiator ends
    }

    /**
     * Stops the listen operation by de-registering with the manager
     */
    @Override
    public void stop() {
        nonHL7 = connProps.getBooleanProperty("nonHL7", false);
        if (nonHL7) {
            logger.info("Non HL7 Netty TCP UnmanagedListenOperation.stop");
            simpleNettyServerBootstrap.close();
        } else {
            logger.info("HL7 Netty TCP UnmanagedListenOperation.stop");
            simpleNettyServerBootstrapHL7.close();
        }
    }

    @Override
    public boolean isSingleton() {
        return false;
    }

    @Override
    public void exceptionThrown(Exception e) {
        logger.info("MLLP UnmanagedListenOperation.exceptionThrown: " + e.getMessage());
        _listener.submit(e);
        this.stop();
    }

}
