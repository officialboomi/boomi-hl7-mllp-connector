package com.boomi.connector.mllp;

import java.io.ByteArrayInputStream;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;

import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.listen.ListenerExecutionResult;
import com.boomi.connector.api.listen.SubmitOptions;
import com.boomi.connector.api.listen.options.WaitMode;
import com.boomi.util.LogUtil;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class SimpleTCPChannelHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger LOGGER = LogUtil.getLogger(SimpleTCPChannelHandler.class);
    Listener _listener;
    String _MLLPWrapper;
    //	KeyManagerFactory _kmf;
    int _maxFileSize;
    boolean _functionalAck;
    KeyManagerFactory _kmf;
    String _nonHL7Response;
    String _nonHL7ResponseError;
    private static final int MIN_MESSAGE_SIZE = 10;
    int _port;
    String _isEDIX12;

    public SimpleTCPChannelHandler(Listener listener, String MLLPWrapper, int maxFileSize, boolean msgAffinity,
                                   KeyManagerFactory kmf, String nonHL7Response, String nonHL7ResponseError, int port, String isEDIX12) {
        _listener = listener;
        _MLLPWrapper = MLLPWrapper;
        _maxFileSize = maxFileSize;
        _functionalAck = msgAffinity;
        _kmf = kmf;
        _nonHL7Response = nonHL7Response;
        _nonHL7ResponseError = nonHL7ResponseError;
        _port = port;
        _isEDIX12 = isEDIX12;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String request) throws Exception {

//		logger.info(ctx.channel().remoteAddress() + s);

        byte[] bytes = null;
        if (!request.isEmpty()) {
            final byte[] utf8Bytes = request.getBytes(StandardCharsets.UTF_8);

            // File size check start
            LOGGER.info("fileSize " + utf8Bytes.length);
            LOGGER.info("maxFileSize " + _maxFileSize);
            if (utf8Bytes.length > _maxFileSize) {
                Exception eSize = new Exception("File  Size exceeding the limit");
                // logger.severe("File Size exceeding the limit");
                String returnWrapperMessage = addMLLPFrameNonHL7(_nonHL7ResponseError);
                if (_MLLPWrapper.equals("Yes")) {
                    LOGGER.info("Non HL7 response message : " + returnWrapperMessage);
                    _listener.submit(eSize);
                    ctx.channel().writeAndFlush(returnWrapperMessage);
                    throw new Exception(eSize);
                } else
                    LOGGER.info("Non HL7 response message : " + _nonHL7ResponseError);
                _listener.submit(eSize);
                ctx.channel().writeAndFlush(_nonHL7ResponseError);
                throw new Exception(eSize);
            }
            // File size check ends

            if (_isEDIX12.equals("Yes")) {
                if (request.substring(0, 3).equals("ISA")) {
                    char elementSeperator = getNthCharacter(request, 3);
                    char segmentDelimiter = getNthCharacter(request, 105);
                    String[] lines = request.split("\\" + String.valueOf(segmentDelimiter));

                    if (lines.length >= 2) {
                        String firstLine = lines[0];
                        LOGGER.warning(firstLine);
                        String lastLine = lines[lines.length - 1]; // Last line is now the last element of the array
                        LocalDate currentDate = LocalDate.now();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
                        String currentDateString = currentDate.format(formatter);

                        String firstLineNew = firstLine + segmentDelimiter;
                        String secondLineNew = "TA1" + elementSeperator + getISA12FromISA(request) + elementSeperator + currentDateString + elementSeperator + "0000" + elementSeperator + "A" + segmentDelimiter;
                        String lastLineNew = lastLine + segmentDelimiter;
                        _nonHL7Response = firstLineNew + secondLineNew + lastLineNew;
                    } else {
                        LOGGER.warning("Invalid X12 message. Expected at least 2 lines.");
                    }
                } else {
                    LOGGER.severe("Not an EDI X12 message");
                }
            }
            if (_functionalAck) {
                SubmitOptions submitOptions = new SubmitOptions().withWaitMode(WaitMode.PROCESS_COMPLETION);
                Future<ListenerExecutionResult> future = _listener
                        .submit(PayloadUtil.toPayload(new ByteArrayInputStream(request.trim().getBytes())), submitOptions);

                // StringBuilder str = new StringBuilder();
                while (!future.isDone()) {
                }
                if (future.get().isSuccess()) { //Process Execution successful?
                    if (_MLLPWrapper.equals("Yes")) {
                        String strMLLP = addMLLPFrameNonHL7(_nonHL7Response);
                        LOGGER.info("Non HL7 response message : " + strMLLP);
                        ctx.channel().writeAndFlush(strMLLP);
                    } else {
                        LOGGER.info("Non HL7 response message : " + _nonHL7Response);
                        ctx.channel().writeAndFlush(_nonHL7Response);
                    }
                } else {
                    if (_MLLPWrapper.equals("Yes")) {
                        String strMLLP = addMLLPFrameNonHL7(_nonHL7ResponseError);
                        LOGGER.info("Non HL7 response message : " + strMLLP);
                        ctx.channel().writeAndFlush(strMLLP);
                    } else {
                        LOGGER.info("Non HL7 response message : " + _nonHL7ResponseError);
                        ctx.channel().writeAndFlush(_nonHL7ResponseError);
                    }

                    // _listener.submit(new Exception("Error processing Non HL7 data"));
                }
            } else {
                _listener.submit(PayloadUtil.toPayload(new ByteArrayInputStream(request.trim().getBytes())));
                if (_MLLPWrapper.equals("Yes")) {
                    String strMLLP = addMLLPFrameNonHL7(_nonHL7Response);
                    LOGGER.info("Non HL7 response message : " + strMLLP);
                    ctx.channel().writeAndFlush(strMLLP);
                } else {
                    LOGGER.info("Non HL7 response message : " + _nonHL7Response);
                    ctx.channel().writeAndFlush(_nonHL7Response);
                }
            }

        }
    }

    private String addMLLPFrameNonHL7(String message) {
        char startChar = 0x0B; // MLLP start character
        char endChar1 = 0x1C; // MLLP end character 1
        char endChar2 = 0x0D; // MLLP end character 2

        return startChar +
                message +
                endChar1 +
                endChar2;
    }

    private static char getNthCharacter(String text, int stringIndex) {
        return text.charAt(stringIndex); // Index 3 corresponds to the 4th character.
    }


    private static String getISA12FromISA(String isaSegment) {
        int start = 90; // Start index of ISA12 (0-based index).
        int end = start + 9; // End index (exclusive).

        if (isaSegment.length() >= end) {
            return isaSegment.substring(start, end).trim();
        } else {
            throw new IllegalArgumentException("Invalid ISA segment. It does not contain ISA12.");
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        // Check if it's an SSL-related exception

        _listener.submit(cause);
        Channel channel = ctx.channel();
        ChannelFuture closeFuture = channel.close();
        closeFuture.addListener(future -> {
            if (future.isSuccess()) {

                if (cause instanceof SSLException) {
                    // Rethrow the SSL exception
                    // ctx.close();
                    throw (SSLException) cause;

                } else {
                    // Handle other exceptions here
                    {
                        LOGGER.severe("Exception caught: " + cause.getMessage());
                        throw (SocketException) cause;
                    }
                    // Perform necessary cleanup or error reporting

                    // Close the connection, if needed

                }

            } else {
                // Failed to close the channel
                LOGGER.severe("Failed to close the channel on");
            }
        });

    }


}
