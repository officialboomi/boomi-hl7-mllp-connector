package com.boomi.connector.mllp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLException;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadUtil;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.api.listen.ListenerExecutionResult;
import com.boomi.connector.api.listen.SubmitOptions;
import com.boomi.connector.api.listen.options.WaitMode;
import com.boomi.util.IOUtil;
import com.boomi.util.LogUtil;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.validation.impl.ValidationContextFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class SimpleTCPChannelHandlerHL7 extends SimpleChannelInboundHandler<String> {

    private static final Logger LOGGER = LogUtil.getLogger(SimpleTCPChannelHandlerHL7.class);
    Listener _listener;
    //	String _MLLPWrapper;
//	KeyManagerFactory _kmf;
    OperationContext _operationContext;
    HapiContext _hapiContext;
    boolean ackAR = false;
    public boolean _autoAcknowledge;
    int _maxFileSize;
    boolean _msgAffinity;
    String _validationLevel;
    // MSH3 Sending Application
    String _MSH3;
    // MSH-4 Sending Facility
    String _MSH4;
    // MSH-5 Receiving Application
    String _MSH5;
    // MSH-6 Receiving Facility
    String _MSH6;
    // MSH-12 Version ID
    String _MSH12;
    String _hL7Delimiter;
    KeyManagerFactory _kmf;
    boolean _useSecureConnection;
    int _port;
    // MLLP frame size
    private static final int MIN_MESSAGE_SIZE = 10;

    public SimpleTCPChannelHandlerHL7(Listener listener, OperationContext operationContext, HapiContext context,
                                      boolean autoAcknowledge, int maxFileSize, boolean msgAffinity, String validationLevel, String mSH3,
                                      String mSH4, String mSH5, String mSH6, String mSH12, String hL7Delimiter, KeyManagerFactory kmf,
                                      boolean useSecureConnection, int port) {
        _listener = listener;
//		_MLLPWrapper = MLLPWrapper;
        _operationContext = operationContext;
        _hapiContext = context;
        _autoAcknowledge = autoAcknowledge;
        _maxFileSize = maxFileSize;
        _msgAffinity = msgAffinity;
        _validationLevel = validationLevel;
        _MSH3 = mSH3;
        _MSH4 = mSH4;
        _MSH5 = mSH5;
        _MSH6 = mSH6;
        _MSH12 = mSH12;
        _hL7Delimiter = hL7Delimiter;
        _kmf = kmf;
        _useSecureConnection = useSecureConnection;
        _port = port;
    }

    @Override
    public boolean acceptInboundMessage(Object msg) throws Exception {
        LOGGER.info("acceptInboundMessage:" + msg.getClass().getName());
        return super.acceptInboundMessage(msg);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info(ctx.channel().remoteAddress() + " Channel Active on " + _port);
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        // super.channelInactive(ctx);
        LOGGER.info(ctx.channel().remoteAddress() + " Channel Closed/Inactive on port " + _port);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String request) throws Exception {

        if (!request.isEmpty()) {
            boolean fileSizeError = false;

            // File size check start
            byte[] utf8Bytes = request.getBytes(StandardCharsets.UTF_8);
            LOGGER.info("message Size " + utf8Bytes.length);
            if (utf8Bytes.length > _maxFileSize) {
                fileSizeError = true;
                HL7Exception eSize = new HL7Exception("File  Size exceeding the limit");
                _hapiContext.setValidationContext(ValidationContextFactory.noValidation());
                _hapiContext.getParserConfiguration().setValidating(false);
                PipeParser parserSizeCheck = _hapiContext.getPipeParser();
                Message receivedMessageSizeCheck = parserSizeCheck.parse(request);
                Message returnMessageSizeCheck = receivedMessageSizeCheck.generateACK(AcknowledgmentCode.AR, eSize);
                String sizeError = addMLLPFrameHL7(returnMessageSizeCheck.toString());
                LOGGER.info("MLLP response message : " + sizeError);
                ctx.channel().writeAndFlush(sizeError);
                IOUtil.closeQuietly();
                _listener.submit(eSize);
                eSize.printStackTrace();
            }
            // File size check end

            // Start Check for File Parsing Errors

            if (!fileSizeError) {

                boolean parsingError = false;
                String parsingErrorMessage = "";
                // use parseRequest only to check if parsing is successful
                Message parseRequest = null;
                try {
                    PipeParser parser;

                    if ("1".equals(_validationLevel)) {
                        LOGGER.info("HL7 Validation = false");
                        _hapiContext.setValidationContext(ValidationContextFactory.noValidation());
                        _hapiContext.getParserConfiguration().setValidating(false);
                        parser = _hapiContext.getPipeParser();
                        parseRequest = parser.parse(request);

                    } else if ("2".equals(_validationLevel)) {
                        LOGGER.info("HL7 Validation = true");
                        _hapiContext.setValidationContext(ValidationContextFactory.defaultValidation());
                        _hapiContext.getParserConfiguration().setValidating(true);
                        parser = _hapiContext.getPipeParser();
                        parseRequest = parser.parse(request);

                    }
                } catch (Exception hL7Exception) {
                    parsingError = true;
                    _hapiContext.setValidationContext(ValidationContextFactory.noValidation());
                    _hapiContext.getParserConfiguration().setValidating(false);
                    parsingErrorMessage = hL7Exception.toString();

                }

                String processBoomiExec;

                if (!_autoAcknowledge) {
                    // if _autoAcknowledge = false
                    if (parsingError) {
                        try {
                            // Generate AR
                            LOGGER.info("_autoAcknowledge = false && encountered parsing error");
                            _hapiContext.setValidationContext(ValidationContextFactory.noValidation());
                            _hapiContext.getParserConfiguration().setValidating(false);
                            PipeParser parserParsingError = _hapiContext.getPipeParser();
                            Message receivedMessageParseError = parserParsingError.parse(request);
                            HL7Exception parsingException = new HL7Exception(parsingErrorMessage);
                            Message returnMessageParseError = receivedMessageParseError
                                    .generateACK(AcknowledgmentCode.AE, parsingException);
                            LOGGER.info("MLLP response message : " + returnMessageParseError.toString());
                            ctx.channel().writeAndFlush(returnMessageParseError.toString());
                            IOUtil.closeQuietly();
                        } catch (HL7Exception manualARError) {
                            // Generate manual AR
                            String returnMessageParseErrorHL7 = manualACK_HL7(request, "AE", parsingErrorMessage);
                            String returnMessageMLLP = addMLLPFrameHL7(returnMessageParseErrorHL7);
                            LOGGER.info("MLLP response message : " + returnMessageMLLP);
                            ctx.channel().writeAndFlush(returnMessageMLLP);
                            IOUtil.closeQuietly();
                            manualARError.printStackTrace();
                        }
                    } else {
                        // No parsing error - Proceed Process Submission
                        // Generate AA or AE
                        processBoomiExec = submitHL7(request, true);
                        processBoomiExec = addMLLPFrameHL7(processBoomiExec);
                        LOGGER.info("MLLP response message : " + processBoomiExec);
                        ctx.channel().writeAndFlush(processBoomiExec);
                        IOUtil.closeQuietly();
                    }
                } else {
                    // if _autoAcknowledge = true
                    // Proceed Process Submission

                    try {
                        processBoomiExec = submitHL7(request, true);
                        processBoomiExec = addMLLPFrameHL7(processBoomiExec);
                        LOGGER.info("MLLP response message : " + processBoomiExec);
                        ctx.channel().writeAndFlush(processBoomiExec);
                        IOUtil.closeQuietly();
                        // Generate AA
                    } catch (Exception autoAAError) {
                        // Generate manual AA, unable to parse
                        processBoomiExec = submitHL7(request, false);
                        String returnMessageManual = manualACK_HL7(request, "AA", "");
                        String returnMessageManualAA = addMLLPFrameHL7(returnMessageManual);
                        LOGGER.info("MLLP response message : " + returnMessageManualAA);
                        ctx.channel().writeAndFlush(returnMessageManualAA);
                        IOUtil.closeQuietly();
                    }

                }
            }
        }
    }

    private String submitHL7(String request, boolean okToParse) throws HL7Exception {
        _hapiContext.setValidationContext(ValidationContextFactory.noValidation());
        _hapiContext.getParserConfiguration().setValidating(false);
        PipeParser parserBoomiExecResponse = _hapiContext.getPipeParser();
        Message parserBoomiExecHL7 = null;
        Message generateResponseMessage = null;

        if (okToParse){
            parserBoomiExecHL7 = parserBoomiExecResponse.parse(request);
        }

        String requestHL7 = trimMLLPFrame(request);
        String fieldSeparator = "\\" + requestHL7.charAt(3);

        String APPLICATION_SENDER_ID;
        String FACILITY_SENDER_ID;
        String APPLICATION_RECEIVER_ID;
        String FACILITY_RECEIVER_ID;
        String PROCESSING_ID;
        PayloadMetadata payloadMetadata = _operationContext.createMetadata();
        String[] lines = requestHL7.split("\\R");
        if (lines.length > 0) {

            String[] headers = lines[0].split(fieldSeparator);
            if (headers.length > 5) {
                if ("MSH".contentEquals(headers[0])) {
                    // Auto ACK segment changes starts

                    APPLICATION_SENDER_ID = headers[4];
                    FACILITY_SENDER_ID = headers[5];
                    APPLICATION_RECEIVER_ID = headers[2];
                    FACILITY_RECEIVER_ID = headers[3];
                    if (headers.length > 8 && headers[9].length() > 0){
                    PROCESSING_ID = headers[9];
                    }
                    else {
                        PROCESSING_ID = generateRandomId();
                    }
                    payloadMetadata.setTrackedProperty("APPLICATION_SENDER_ID", APPLICATION_SENDER_ID);
                    payloadMetadata.setTrackedProperty("FACILITY_SENDER_ID", FACILITY_SENDER_ID);
                    payloadMetadata.setTrackedProperty("APPLICATION_RECEIVER_ID", APPLICATION_RECEIVER_ID);
                    payloadMetadata.setTrackedProperty("FACILITY_RECEIVER_ID", FACILITY_RECEIVER_ID);
                    payloadMetadata.setTrackedProperty("PROCESSING_ID", PROCESSING_ID);
                    payloadMetadata.setTrackedProperty("MLLP_RESPONSE", "");
                }
                Future<ListenerExecutionResult> future;
                StringBuilder response = new StringBuilder();
                try {

                    if (_msgAffinity) {
                        SubmitOptions submitOptions = new SubmitOptions().withWaitMode(WaitMode.PROCESS_COMPLETION);
                        future = _listener.submit(
                                PayloadUtil.toPayload(new ByteArrayInputStream(requestHL7.getBytes()), payloadMetadata),
                                submitOptions);

                        while (!future.isDone()) {
                        }

                        LOGGER.info("Execution ID: " + future.get().getExecutionId() + " SUCCESS STATUS: "
                                + future.get().isSuccess());
                        if (future.get().isSuccess()) {
                            if (okToParse) {
                                generateResponseMessage = parserBoomiExecHL7.generateACK();
                            }
                        } else {

                            String responseFile = MLLPUnmanagedListenOperation.workDir + "/"
                                    + future.get().getExecutionId() + ".response";
                            Path path = Paths.get(responseFile);
                                if (path.toFile().exists()) {
                                LOGGER.severe("Error File exists");
                                ackAR = true;
                                try (Stream<String> stream = Files.lines(path,
                                        StandardCharsets.UTF_8)) {
//                    
                                    stream.forEach(s -> response.append(s).append("\\R"));
                                     String[] tokens = response.toString().split(Pattern.quote("||"));
                                    String errorCode = tokens[0];
                                    String errorMessage = tokens[1];

                                    HL7Exception e1 = new HL7Exception(errorMessage);

                                    if (okToParse) {
                                        if (errorCode.equals("AE")) {

                                            generateResponseMessage = parserBoomiExecHL7
                                                    .generateACK(AcknowledgmentCode.AE, e1);
                                        } else if (errorCode.equals("AR")) {
                                            generateResponseMessage = parserBoomiExecHL7
                                                    .generateACK(AcknowledgmentCode.AR, e1);
                                        } else {
                                            generateResponseMessage = parserBoomiExecHL7
                                                    .generateACK(AcknowledgmentCode.AE, e1);
                                        }
                                    }
                                    LOGGER.info("Deleting file from " + responseFile);
                                    File fileToDelete = new File(responseFile);
                                    if (fileToDelete.delete()) {
                                        LOGGER.info("Deleted file successfully" + responseFile);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (HL7Exception e) {
                                     e.printStackTrace();
                                }

                            } else {
                                HL7Exception e1 = new HL7Exception("Unknown Error");
                                try {
                                    if (okToParse)
                                        generateResponseMessage = parserBoomiExecHL7.generateACK(AcknowledgmentCode.AE,
                                                e1);
                                } catch (HL7Exception e) {
                                    // TODO Auto-generated catch block
                                    LOGGER.severe("Unable to parse the HL7 message");
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                            }
                        }
                    } else {
                        SubmitOptions submitOptionsNoFuncAck = new SubmitOptions()
                                .withWaitMode(WaitMode.PROCESS_SUBMISSION);
                        _listener.submit(PayloadUtil.toPayload(new ByteArrayInputStream(request.getBytes()),
                                payloadMetadata), submitOptionsNoFuncAck);
                        if (okToParse)
                            generateResponseMessage = parserBoomiExecHL7.generateACK();
                    }
                } catch (InterruptedException e2) {
                    LOGGER.severe("InterruptedException" + e2);
                    e2.printStackTrace();
                } catch (ExecutionException e2) {
                    LOGGER.severe("ExecutionException" + e2);
                    e2.printStackTrace();
                } catch (HL7Exception e) {
                    LOGGER.severe("HL7Exception" + e);
                    e.printStackTrace();
                } catch (IOException e) {
                    LOGGER.severe("IOException" + e);
                    e.printStackTrace();
                }
            }
        }
        if (okToParse) {
            LOGGER.warning(generateResponseMessage.toString());
            return generateResponseMessage.toString();
        } else
            return "";
    }

    public static String generateRandomId() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    private String manualACK_HL7(String mllpMessage, String ackCode, String ErrMsg) {
        String resultACK_HL7 = "";

        String requestHL7 = trimMLLPFrame(mllpMessage);

        String fieldSeparator = Character.toString(requestHL7.charAt(3));

        String ENCODING_CHARACTERS = "^~&";
        String APPLICATION_SENDER_ID = _MSH3;
        String FACILITY_SENDER_ID = _MSH4;
        String APPLICATION_RECEIVER_ID = _MSH5;
        String FACILITY_RECEIVER_ID = _MSH6;
        // if (headers.length>10)
        String PROCESSING_ID;
        String Processing_ID = null;
        String Version_ID = null;

        String[] lines = requestHL7.split("\\R");
        if (lines.length > 0) {
            String[] headers = lines[0].split("\\\\" + fieldSeparator);

            if (headers.length > 5) {
                if ("MSH".contentEquals(headers[0])) {
                    if (headers[1].length() > 0)
                        ENCODING_CHARACTERS = headers[1];
                    if (headers[4].length() > 0)
                        APPLICATION_SENDER_ID = headers[4];
                    if (headers[5].length() > 0)
                        FACILITY_SENDER_ID = headers[5];
                    if (headers[2].length() > 0)
                        APPLICATION_RECEIVER_ID = headers[2];
                    if (headers[3].length() > 0)
                        FACILITY_RECEIVER_ID = headers[3];
                }
            }

            if (headers.length > 9 && headers[9].length() > 0)
                PROCESSING_ID = headers[9];
            else
                PROCESSING_ID = generateRandomId();

            if (headers.length > 10 && headers[10].length() > 0)
                Processing_ID = headers[10];
            if (headers.length > 11 && headers[11].length() > 0)
                Version_ID = headers[11];

            StringBuilder responseBuilder = new StringBuilder();

            responseBuilder.append("MSH").append(fieldSeparator);
            responseBuilder.append(ENCODING_CHARACTERS).append(fieldSeparator);
            responseBuilder.append(APPLICATION_SENDER_ID).append(fieldSeparator);
            responseBuilder.append(FACILITY_SENDER_ID).append(fieldSeparator);
            responseBuilder.append(APPLICATION_RECEIVER_ID).append(fieldSeparator);
            responseBuilder.append(FACILITY_RECEIVER_ID).append(fieldSeparator);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = new Date();
            responseBuilder.append(dateFormat.format(date)).append(fieldSeparator);
            responseBuilder.append(fieldSeparator);
            responseBuilder.append("ACK").append(fieldSeparator);
            responseBuilder.append(PROCESSING_ID).append(fieldSeparator);
            responseBuilder.append(Processing_ID).append(fieldSeparator);
            responseBuilder.append(Version_ID).append(System.lineSeparator());

            if (ackCode.equals("AA"))
                responseBuilder.append("MSA").append(fieldSeparator).append("AA").append(fieldSeparator)
                        .append(PROCESSING_ID).append(System.lineSeparator());
            else {
                String MSA = "MSA" + fieldSeparator + ackCode + fieldSeparator;
                responseBuilder.append(MSA).append(PROCESSING_ID).append(System.lineSeparator());
                ErrMsg = ErrMsg.replaceAll("ca\\.uhn\\.hl7v2\\.HL7Exception:", "");
                ErrMsg = ErrMsg.replaceAll("ca\\.uhn\\.hl7v2\\.parser\\.EncodingNotSupportedException:", "");
                String ERR = "ERR" + fieldSeparator + ErrMsg;
                responseBuilder.append(ERR).append(System.lineSeparator());
            }
            resultACK_HL7 = responseBuilder.toString();

        }
        return resultACK_HL7;

    }

    private String trimMLLPFrame(String mllpMessage) {
        // Find the start and end marker positions
        int startMarkerIndex = mllpMessage.indexOf((char) 0x0B);
        int endMarkerIndex = mllpMessage.lastIndexOf((char) 0x1C);
        // Check if the start and end markers are present
        if (startMarkerIndex != -1 && endMarkerIndex != -1) {
            // Trim the MLLP frame and return the message content
            return mllpMessage.substring(startMarkerIndex + 1, endMarkerIndex);
        }
        // Return the original message if the markers are not found
        return mllpMessage;
    }

    public static String addMLLPFrameHL7(String message) {
        char startChar = 0x0B; // MLLP start character
        char endChar1 = 0x1C; // MLLP end character 1
        char endChar2 = 0x0D; // MLLP end character 2

        return startChar +
                message +
                endChar1 +
                endChar2;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        // Check if it's an SSL-related exception
        LOGGER.severe(cause.toString());
        _listener.submit(cause);
        Channel channel = ctx.channel();
        ChannelFuture closeFuture = channel.close();
        closeFuture.addListener(future -> {
            if (future.isSuccess()) {
            if (cause instanceof SSLException) {
                    // Rethrow the SSL exception
                    // ctx.close();
                    throw (SSLException) cause;
                } else {
                    LOGGER.severe("Exception caught: " + cause.getMessage());
                    throw (SocketException) cause;
                }

            }
        });

    }

}
