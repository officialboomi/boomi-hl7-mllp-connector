package com.boomi.connector.mllp;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import com.boomi.connector.api.PublicKeyStore;

public class MockPublicKeyStore implements PublicKeyStore{
	
	private KeyStore publicKeyStore;
	private String filePath;

	MockPublicKeyStore(String filePath)
	{
		this.filePath = filePath;
		this.publicKeyStore = loadX509(); //Generate local Keystore
	}
	
	@Override
	public KeyStore getKeyStore() {
		return this.publicKeyStore;
	}

	@Override
	public Object getUniqueId() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	private KeyStore loadX509(){
		FileInputStream in = null;
		FileOutputStream out = null;
		KeyStore keystore = null;
		
		try {
			String storeName = "PublicKeyStore.jks";
		    String storeType = "jks";
		    try (FileOutputStream fileOutputStream = new FileOutputStream(storeName)) {
		      keystore = KeyStore.getInstance(storeType);
		      keystore.load(null, "test123".toCharArray());
		      keystore.store(fileOutputStream, "test123".toCharArray());
		    } catch (CertificateException | NoSuchAlgorithmException | IOException | KeyStoreException e) {
		    	e.printStackTrace();}  
		    finally {   }
		   


			String alias = "certalias";
			char[] password = "test123".toCharArray();

			//////

			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			InputStream certstream = fullStream (filePath);
			Certificate certs =  cf.generateCertificate(certstream);

			///
			File keystoreFile = new File("/src/test/resources/PublicKeyStore.jks");
			// Load the keystore contents
			in = new FileInputStream(keystoreFile);
			keystore.load(in, password);


			// Add the certificate
			keystore.setCertificateEntry(alias, certs);

			// Save the new keystore contents
			out = new FileOutputStream(keystoreFile);
			keystore.store(out, password);
			
			//System.out.println("keystore with Public certificate loaded" + keystore.getCertificate(alias).toString());


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(in != null)
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			if(out != null)
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return keystore; 
	}

	private static InputStream fullStream ( String fname ) throws IOException {
		FileInputStream fis = new FileInputStream(fname);
		DataInputStream dis = new DataInputStream(fis);
		byte[] bytes = new byte[dis.available()];
		dis.readFully(bytes);
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		return bais;
	}
}


