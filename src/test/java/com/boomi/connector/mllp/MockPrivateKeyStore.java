package com.boomi.connector.mllp;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.UUID;

import com.boomi.connector.api.PrivateKeyStore;

public class MockPrivateKeyStore implements PrivateKeyStore{
	
	String password;
	String filePath;
	String id;
	KeyStore privateKeyStore;
	
	MockPrivateKeyStore(String filePath, String password)
	{
		this.filePath=filePath;
		this.password=password;
		this.id = UUID.randomUUID().toString();
		this.privateKeyStore = loadPrivateX509(); //Generate local Keystore
	}
	
	@Override
	public KeyStore getKeyStore() {
		return privateKeyStore;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Object getUniqueId() {
		// TODO Auto-generated method stub
		return id;
	}

	private KeyStore loadPrivateX509(){
		
		KeyStore keystore = null;
		try {
			keystore = KeyStore.getInstance("pkcs12");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			keystore.load(new FileInputStream(filePath), password.toCharArray());

			//System.out.println("keystore with Private Key loaded + " + "type = " + keystore.getCertificate("bogus") );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
		
		return keystore; 
	}
	
}
