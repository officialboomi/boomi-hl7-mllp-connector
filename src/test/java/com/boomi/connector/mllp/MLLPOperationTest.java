// Copyright (c) 2013 Boomi, Inc.

package com.boomi.connector.mllp;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.lang.ArrayUtils;
import org.junit.jupiter.api.*;

import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PrivateKeyStore;
import com.boomi.connector.api.PublicKeyStore;
import com.boomi.connector.api.listen.Listener;
import com.boomi.connector.testutil.ConnectorTester;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.hoh.sockets.CustomCertificateTlsSocketFactory;
import ca.uhn.hl7v2.hoh.util.HapiSocketTlsFactoryWrapper;
import ca.uhn.hl7v2.hoh.util.KeystoreUtils;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v24.message.ACK;
import ca.uhn.hl7v2.model.v24.message.ADT_A01;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.validation.impl.ValidationContextFactory;

/**
 * @author Dave Hock
 *
 *         01/09/2020(Prem): Refactored to work with the Threadpool by ordering
 *         the method exection
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MLLPOperationTest {

	static MLLPUnmanagedListenOperation _operation;
//    static final int _portNum = 9909;
	static final int _portNum = 9901;
	// static final int _portNumTLS = 9910;
	static final int _portNumTLS = 9919;
//  static final int _portNumTLS2 = 9914;
	// static final int _portNumTLS2 = 9922;
	static final int _portNumTLS2 = 9990;
//    static final int _portNumNonHL7 = 9918;
//    static final int _portNumNonHL7 = 9923;
	static final int _portNumNonHL7 = 9925;
	static final int _portNumNonHL7TLS = 9930;
	Logger logger = Logger.getLogger(MLLPOperationTest.class.getName());

	@Test
	@Order(1)
	public void testStartListener() throws InterruptedException {
	//	new Thread(new TestListener(_portNum)).start();
	}

	@Test
	@Order(2)
	public void testExecuteOperationNonTLS() throws Exception {
		int howManyThreads = 1;

		for (int i = 0; i < howManyThreads; i++) {

//            new TestMLLPClient(_portNum).start();
	//		new TestMLLPClientNoTLS(_portNum).start();
		}
	//	Thread.sleep(5000);
	}

	@Test
	@Order(3)
	public void testStartListenerTLS1way() throws InterruptedException {
	//	 new Thread(new TestListenerTLS1way(_portNumTLS)).start();
	}

	@Test
	@Order(4)
	public void testExecuteOperationAsyncTLS() throws Exception {
		int howManyThreads = 1;

		// int repeat = 100;

		for (int i = 0; i < howManyThreads; i++) {

//			 new TestMLLPClientTLS1way(_portNumTLS).start();
			// new TestMLLPClientTLS1wayLoad(_portNumTLS).start();
//			 Thread.sleep(2000);
		}

		// Thread.sleep(20000);
	}

	@Test
	@Order(5)
	public void testStartListenerTLS2way() throws InterruptedException {
		// new Thread(new TestListenerTLS2way(_portNumTLS2)).start();
	}

	@Test
	@Order(6)
	public void testExecuteOperationAsyncTLS2way() throws Exception {
		int howManyThreads = 1;

		for (int i = 0; i < howManyThreads; i++) {

			// new TestMLLPClientTLS2way(_portNumTLS2).start();
			// Thread.sleep(5000);
		}

	}

	@Test
	@Order(7)
	public void testStartListenerNonHL7() throws InterruptedException {
		// new Thread(new TestStartListenerNonHL7(_portNumNonHL7)).start();
		// Thread.sleep(100000);
	}

	@Test
	@Order(8)
	public void testExecuteOperationNonTLS2() throws Exception {
		int howManyThreads = 1;

		for (int i = 0; i < howManyThreads; i++) {
//			Thread.sleep(1000);
//            new TestMLLPClient(_portNum).start();
			// new TestMLLPClientNoTLS(_portNumNonHL7).start();
		}
		// Thread.sleep(5000);
	}

	@Test
	@Order(9)
	public void testMLLPnonHL7Client() throws Exception

	{
		int howManyThreads = 1;
//		Thread.sleep(1000);
		for (int i = 0; i < howManyThreads; i++) {
			 //new TestMLLPnonHL7Client(9941).start();
			new TestMLLPnonHL7Client(_portNum).start();
			
		}
		 Thread.sleep(5000);
	}

	@Test
	@Order(10)
	public void testStartListenerNonHL7TLS() throws InterruptedException {
		// new Thread(new TestStartListenerNonHL7TLS(_portNumNonHL7TLS)).start();
	}

	@Test
	@Order(11)
	public void testMLLPnonHL7TLSClient() throws Exception {
		int howManyThreads = 1;

		for (int i = 0; i < howManyThreads; i++) {

			// new TestMLLPClientTLS1way(_portNumNonHL7TLS).start();
			// new TestMLLPnonHL7TLSClient(_portNumNonHL7TLS).start();
		}
		// Thread.sleep(2000);
	}

}

class TestListener implements Runnable {

	static Listener _listener;
	private long _portNum;
	private long _fileSize = 1000;
	static final long _threadPoolSize = 1;
	static MLLPUnmanagedListenOperation _operation;

	public TestListener(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestListener");
		_listener = new MockListener();

		MLLPConnector connector = new MLLPConnector();
		ConnectorTester tester = new ConnectorTester(connector);

		Map<String, Object> connProps = new HashMap<String, Object>();

		connProps.put("portNumber", (Long) _portNum);
		connProps.put("messageSize", (Long) _fileSize);
		connProps.put("useSecureConnection", false);
	//	connProps.put("selectCipherSuite", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384");
		connProps.put("setCipherSuite", "");
		connProps.put("validationLevel", "1");
//		connProps.put("mllpAck", "No");
		connProps.put("nonHL7MLLPWrapper", "Yes");
		connProps.put("nonHL7", false);
		connProps.put("autoAcknowledge", false);
		connProps.put("MSH3", "MSH3");
		connProps.put("MSH4", "MSH4");
		connProps.put("MSH5", "MSH5");
		connProps.put("MSH6", "MSH3");
		connProps.put("MSH12", "2.3.1");
		
		
//		connProps.put("workDir", "C:\\Boomi AtomSphere\\Atom\\Boomi AtomSphere\\work\\mllp\\");

		Map<String, Object> opProps = new HashMap<String, Object>();
		tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
		_operation = new MLLPUnmanagedListenOperation(tester.getOperationContext());
		_operation.start(_listener);
//        try {
//			Thread.sleep(260000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        _operation.stop();
	}
}

class TestMLLPClientNoTLS extends Thread {

	private int _portNum;
	private static final String message3 = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-4458|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";

	private static final String message = "MSH|^~\\&|KPHC|NW|HIE|KPO|20220408164120|411|ADT^A18^ADT_A18|4|D|2.3.1\r"
			+ "EVN|A18|20140113134120|||^BAAAAAAAAWEBER^PLIS^^^^^^NW^^^^^PC\r"
			+ "PID|1||71091422^^^KP139999^MR||KPNWS^FAAAAAAACEOK|DLA|19990909|F||U^UNKNOWN^AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA|99999 YUMMYAAA LANE^^WASHAAAAAA^WA^98671^USA^RS^^050|050|(360)997-8013^P^7^^^999^9998013~(555)555-5555^P^7^^^555^5555555|(360)999-3131X5395^P^8^^^360^8553131^9995^^X~(503)999-2394^P^8^^^503^9999394||U^UNKNOWNAAAAAA|||999-99-6832|||30^IAN||||||||N\r"
			+ "PD1|||MEDICAL OFFICEAAAAAA^^109906|99993^DAHLAAAAA^LINEAAAAA^FIELDAAAAA^^^^^KP1084^^^^PRN\r"
			+ "MRG|E1909999980^^^^EPI~9999-44-44~99994444^^^KP1039^MR|||E9999826380^^^^EPI~9999-44-44~99994444^^^KP1039^MR\r"
			+ "";

	private static final String messageN = "MSH|^~\\&|EPIC|REG_UPDATE|GUNDERSEN|GUNDERSEN|20220705155003|000001|ADT^A28^ADT_A05|84951|T|2.4\r"
			+ "EVN|A28|20220705155003||REG_UPDATE|000001^LastName^SARAH^J^^^^^GHS^^^^^LAC||\r"
			+ "PID|1|     Z2883|000008437113^^^MR^MR||ZZZTEST^MENA||20080705|F|||^^^^^USA^H||||||||123-55-1234|||||||||||N||\r"
			+ "ZPD||MYCH|||||N|||||||||||N||||||F|||||||||\r" + "PD1|||GL LA CROSSE^^101010|||||||||||||||\r"
			+ "PV1|1||||||||||||||||||||||||||||||||||||||||||||||||||^^^|||||||||||\r" + "";

	private static final String messageX = "MSH|^~\\&|EXT|EXT|UHC|UHC|20210714080959||ORM^O01|Q1223456789X123456|P|2.3\r"
			+ "PID|1|61322^^^SSH^MR^HABS|41322^^^SSH^MR^HABS|31074860912^^^AUSHIC^MC^HABS^^1221|WOMBAT^Susan^^^MRS^^R||19670422|F||NOT ABORIGINAL OR TORRES STRAIT ISLANDER|7 TESTING COURT^^DUNDOWRAN^QLD^4655^AU^H^^||^PRN^|||M||5191111^^^UCH^FIN^HABS||||CD:177518098|||0|AUSTRALIA||AUSTRALIA||V1|1|I|Periop^^^SSH^^^SSH|C|||2251337X^Kostic^Miso^^^^^^SSH^PRSNL^^^ODR^||^RANATUNGE^DENAGAMUWE^RAVINDRA^^Dr^^^EXTERNAL_ID^PRSNL^^^BUILD_ERROR^|DAY SURGERY|Periop|||PRIVATE MEDICAL PRACTITIONER|W1||2251337X^Kostic^Miso^^^^^^SSH^PRSNL^^^ODR^|D|5191111^^^UCH^FIN^HABS|PI|||||||||||||||||||SSH||A|||20210714065900\r"
			+ "IN1|1|19191723^CBHS PLAN^^^CBHS PLAN|133|CBHS Friendly Society|Locked Bag 5014^^PARRAMATTA^NSW^2124^AU|Monique Ferraro|^^|||1|The Wesley Hospital||||21001231000000|WOMBAT^Susan^^^MRS^^R|SELF|19670422|^1221^^^^|||1|||||||||||||PI|2890502||||||HOME DUTIES|F|451 Coronation Dve^^AUCHENFLOWER^QLD^4066^AU|||||2890502\r"
			+ "ORC|NW|264814307^HNAM_ORDERID||264814307|NW||||20210714080957|^Jacob^Jino^^^^^^^PRSNL^^^BUILD_ERROR||2251337X^Kostic^Miso^^^^^^SSH^PRSNL^^^ODR^|||20210714080959|LABCOLLECTED||Initiate plan/Conditional^Initiate Plan/Conditional/Paper|^Jacob^Jino^^^^^^^PRSNL^^^BUILD_ERROR\r"
			+ "OBR|1|264814307^HNAM_ORDERID||HM^Histo|||20210714080900||||O|||20210714080900|HISTOLOGY&Histology^^Gastric Biopsy Upper GIT|2251337X^Kostic^Miso^^^^^^SSH^PRSNL^^^ODR^||||000002021195000024^HNA_ACCN||20210714080959||General Lab|||1^^0^20210714080900^^R|||||||||20210714080900\r"
			+ "SAC|||892010||1|QML AP Spec|20210714080900||HISTOLOGY|21-195-0024|||||||||||||1|EA||Misc|||||||||||||||||\r"
			;

	public TestMLLPClientNoTLS(int port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestMLLPTLSClientNoTLS");
		long start = System.currentTimeMillis();
		HapiContext context = new DefaultHapiContext();
		context.getParserConfiguration().setValidating(false);
		Connection connection1 = null;
		Connection connection2 = null;

		for (int i = 0; i < 1; i++) {

			try {
				System.out.println("Calling Load Balancer");
				System.out.println(i);
				// connection =
				// context.newClient("ec2-18-219-142-174.us-east-2.compute.amazonaws.com", 9909,
				// false);
				// connection =
				// context.newClient("MLLPLoadbalancerForPAC-e8fca50061e94a89.elb.us-east-2.amazonaws.com",
				// 9909, false);
				connection1 = context.newClient("192.168.0.108", _portNum, false);
		//		connection2 = context.newClient("192.168.0.108", _portNum, false);
			} catch (HL7Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Initiator initiator1 = connection1.getInitiator();
		//	Initiator initiator2 = connection1.getInitiator();

			String remoteAddress = connection1.getRemoteAddress().toString();

			System.out.println("remoteAddress");
			System.out.println(remoteAddress);

			// send the created HL7 message over the connection established
			Parser parser = context.getPipeParser();
			// System.out.println("Sending message:" + "\r" + message1);
			Message response1 = null;
			Message response2 = null;
			ADT_A01 adtMessage = null;

			try {
				response1 = initiator1.sendAndReceive(parser.parse(message));
	//			response2 = initiator2.sendAndReceive(parser.parse(message));
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LLPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// display the message response received from the remote party

			String responseString1 = null;
	//		String responseString2 = null;
			try {
				responseString1 = parser.encode(response1);
	//			responseString2 = parser.encode(response2);
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Received response 1:\r" + responseString1.replaceAll("\r", "\r"));
	//		System.out.println("Received response:\r" + responseString2.replaceAll("\r", "\r"));

			System.out.println("Total Time" + (System.currentTimeMillis() - start));
			// assertEquals("Hello sync response from your listener. Here is your input
			// echoed back: HELLO from JUNIT\r", new
			// String(actual.get(0).getPayloads().get(0)));
		}
	}

	static String inputStreamToString(InputStream is) throws IOException {
		try (Scanner scanner = new Scanner(is, "UTF-8")) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}

class TestMLLPClient extends Thread {

	private int _portNum;
	private static final String messageM = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3458|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";

	private static final String message = "MSH|^~\\&|EPIC|REG_UPDATE|GUNDERSEN|GUNDERSEN|20220705155003|000001|ADT\r"
			+ "^A28^ADT_A05|84951|T|2.4|||||||||||\r"
			+ "EVN|A28|20220705155003||REG_UPDATE|000001^LastName^SARAH^J^^^^^GHS^^^^^LAC||\r"
			+ "PID|1|     Z2883|000008437113^^^MR^MR||ZZZTEST^MENA||20080705|F|||^^^^^USA^H||||||||123-55-1234|||||||||||N||\r"
			+ "ZPD||MYCH|||||N|||||||||||N||||||F|||||||||\r" + "PD1|||GL LA CROSSE^^101010|||||||||||||||\r"
			+ "PV1|1||||||||||||||||||||||||||||||||||||||||||||||||||^^^|||||||||||\r" + "";

	private static final String message2 = message.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");;

	public TestMLLPClient(int port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestMLLPClient");
		long start = System.currentTimeMillis();
		Socket socket;
		try {
			socket = new Socket("192.168.0.102", _portNum);
			// Create input and output streams to read from and write to the server
			PrintStream out = new PrintStream(socket.getOutputStream());
			byte sb = 0x0B;
			byte eb = 0x1C;
			out.print(sb);
			out.print(message2);
			out.print(eb);
			out.flush();
			out.close();
			socket.close();
			// TODO read response
			Thread.sleep(30000);

//	        System.out.println(inputStreamToString(socket.getInputStream()));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Total Time" + (System.currentTimeMillis() - start));
		// assertEquals("Hello sync response from your listener. Here is your input
		// echoed back: HELLO from JUNIT\r", new
		// String(actual.get(0).getPayloads().get(0)));
	}

	static String inputStreamToString(InputStream is) throws IOException {
		try (Scanner scanner = new Scanner(is, "UTF-8")) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}

class TestListenerTLS1way implements Runnable {

	static Listener _listener;
	private long _portNum;
	private long _fileSize = 1;
	static final long _threadPoolSize = 1;
	static MLLPUnmanagedListenOperation _operation;

	public TestListenerTLS1way(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TLS Test Listener");
		_listener = new MockListener();

		MLLPConnector connector = new MLLPConnector();
		ConnectorTester tester = new ConnectorTester(connector);

		Map<String, Object> connProps = new HashMap<String, Object>();
		connProps.put("portNumber", (Long) _portNum);
		connProps.put("useSecureConnection", true);
		// connProps.put("selectCipherSuite", "DEFAULT");
		connProps.put("setCipherSuite", "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384");
		connProps.put("Address", "192.168.0.104,192.168.0.103");
		connProps.put("Toggle_IP_DNS", "IP");
		connProps.put("Whitelisting", false);
		connProps.put("doValidation", true);
		connProps.put("messageSize", (Long) _fileSize);
		connProps.put("validationLevel", "2");
		connProps.put("nonHL7",false);

		// PublicKeyStore partnerPublicSSLCertificate = new
		// MockPublicKeyStore("src/test/resources/ClientPublicCert.cer");
		PrivateKeyStore privateSSLCert = new MockPrivateKeyStore("src/test/resources/ServerApp.pfx", "MLLPserver");
		// connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);
		connProps.put("serverPrivateSSLKey", privateSSLCert);

		Map<String, Object> opProps = new HashMap<String, Object>();
		tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
		_operation = new MLLPUnmanagedListenOperation(tester.getOperationContext());
		_operation.start(_listener);
//        try {
//			Thread.sleep(260000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        _operation.stop();
	}
}

class TestMLLPClientTLS1way extends Thread {

	private int _portNum;

	private static final String messagen = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3458|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";
	private static final String message3 = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3460|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";
	private static final String message2 = "MSH|^~\\&|GHH LAB|ELAB-3|GHH123 OE|BLDG4|200202150930||ORU^R01|CNTRL-3460|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";
	private static final String message1 = "MSH|^~\\&|KPDS|SCA.BEL|CBORD|SCA|20220406021608-0700||ADT^A08^ADT_A01|0910094786|T|2.3.1|||NE|NE\r"
			+ "EVN||2022040602160800-0700||REG_UPDATE|K000000|20220406021613-0700\r"
			+ "PID|1||000011111111^^^KP1013^MR~888888888^^^USSSA^SS||LAST^FIRST||19670101|F|LAST^FIRST|O^Other^HL70005|||^NET^Internet^name@gmail.com|||||220000000000||||N^NONHISPANIC^HL70189||||||USA||N\r"
			+ "PV1|1|E|BDMED^BBG62^62^BEL^D^^80101^^DMED|ER|||7560796^LEE^DAMIEN^BENNETT (M.D.)^^^^^KP1003^^^^PRN|||EM||||||||000|220405148663^^^KP1066^^BEL|113||||||||||||||||||||||||20220405212600\r"
			+ "";

	private static final String messageN = "HELLO BOOMI";

	public TestMLLPClientTLS1way(int port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestMLLPClientTLS1way");
		long start = System.currentTimeMillis();
		HapiContext context1 = new DefaultHapiContext();
		HapiContext context2 = new DefaultHapiContext();
		context1.getParserConfiguration().setValidating(false);
		context2.getParserConfiguration().setValidating(false);

		Map<String, Object> connProps = new HashMap<String, Object>();

		PublicKeyStore partnerPublicSSLCertificate = new MockPublicKeyStore("src/test/resources/ServerPublicCert.cer");
		// PublicKeyStore partnerPublicSSLCertificate = new
		// MockPublicKeyStore("src/test/resources/ClientPublicCert.cer");
		// PublicKeyStore partnerPublicSSLCertificate = new
		// MockPublicKeyStore("src/test/resources/BoomiSSLPK.cer");
		connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);

		KeyStore keyStore = null;
		try {
			keyStore = partnerPublicSSLCertificate.getKeyStore();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			KeyStore.getInstance("PKCS12");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			KeystoreUtils.validateKeystoreForTlsSending(keyStore);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CustomCertificateTlsSocketFactory tlsFac = new CustomCertificateTlsSocketFactory(keyStore, "");
		context1.setSocketFactory(new HapiSocketTlsFactoryWrapper(tlsFac));
		// context2.setSocketFactory(new HapiSocketTlsFactoryWrapper(tlsFac));

		// create a new MLLP client over the specified port
		Connection connection1 = null;
		// Connection connection2 = null;

		for (int i = 0; i < 1; i++) {

			try {
				// new TestMLLPnonHL7Client(_portNumNonHL7).run();
				System.out.println("New Client connection");
				connection1 = context1.newClient("192.168.0.109", _portNum, true);
				// connection1 =
				// context1.newClient("ExtLBFormMolecule-1529742430.us-east-2.elb.amazonaws.com",
				// _portNum, true);
			} catch (HL7Exception e1) {

				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// The initiator which will be used to transmit our message
			Initiator initiator1 = connection1.getInitiator();
		Parser parser1 = context1.getPipeParser();
			Message response1 = null;
	
			try {
				response1 = initiator1.sendAndReceive(parser1.parse(message1));
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LLPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Creation New Client connection error");
				e.printStackTrace();
			}

			// display the message response received from the remote party
			String responseString1 = null;
			// try {
			// response2 = initiator2.sendAndReceive(parser2.parse(message2));
			// } catch (HL7Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (LLPException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			String responseString2 = null;
			try {
				responseString1 = parser1.encode(response1);
				// responseString2 = parser2.encode(response2);
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("Received response 1 :\r" + responseString1.replaceAll("\r", "\r"));
			// System.out.println("Received response 2 :\r" +
			// responseString2.replaceAll("\r", "\r"));

			System.out.println("Total Time" + (System.currentTimeMillis() - start));
			// assertEquals("Hello sync response from your listener. Here is your input
			// echoed back: HELLO from JUNIT\r", new
			// String(actual.get(0).getPayloads().get(0)));
		}

	}

}

class TestListenerTLS2way implements Runnable {

	static Listener _listener;
	private long _portNum;
	private long _fileSize = 3;
	static final long _threadPoolSize = 1;

	static MLLPUnmanagedListenOperation _operation;

	public TestListenerTLS2way(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TLS Test Listener");
		_listener = new MockListener();

		MLLPConnector connector = new MLLPConnector();
		ConnectorTester tester = new ConnectorTester(connector);

		Map<String, Object> connProps = new HashMap<String, Object>();
		connProps.put("portNumber", (Long) _portNum);
		connProps.put("messageSize", (Long) _fileSize);
		connProps.put("useSecureConnection", true);
		connProps.put("selectCipherSuite", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384");
		connProps.put("setCipherSuite", "");
		connProps.put("Address", "192.168.0.100,192.168.0.102,192.168.0.101,192.168.200.239,192.168.0.102");
		connProps.put("Toggle_IP_DNS", "IP");
		connProps.put("Whitelisting", false);
		connProps.put("validationLevel", "2");
//		connProps.put("customACK", "standard");

		connProps.put("maximumConnections", _threadPoolSize);

		// PublicKeyStore partnerPublicSSLCertificate = new
		// MockPublicKeyStore("src/test/resources/client2way.cer");
		// PrivateKeyStore privateSSLCert = new
		// MockPrivateKeyStore("src/test/resources/ServerCert2way.pfx", "server2way");
		// connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);
		// connProps.put("serverPrivateSSLKey", privateSSLCert);

		PublicKeyStore partnerPublicSSLCertificate = new MockPublicKeyStore("src/test/resources/CAcert.cer");
		// PrivateKeyStore serverPrivateSSLKey = new
		// MockPrivateKeyStore("src/test/resources/ServerCert2way.pfx", "server2way");
		PrivateKeyStore serverPrivateSSLKey = new MockPrivateKeyStore("src/test/resources/eisdev.kp.org.pfx",
				"xGw]F<9W>jAR");
		connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);
		connProps.put("serverPrivateSSLKey", serverPrivateSSLKey);

		Map<String, Object> opProps = new HashMap<String, Object>();
		tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
		_operation = new MLLPUnmanagedListenOperation(tester.getOperationContext());
		_operation.start(_listener);
//        try {
//			Thread.sleep(260000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        _operation.stop();
	}
}

class TestMLLPClientTLS2way extends Thread {

	private int _portNum;
	private static final String messageN = "WELCOME TO BOOMI";

	private static final String message = "MSH|^~\\&|EPIC|REG_UPDATE|GUNDERSEN|GUNDERSEN|20220705155003|000001|ADT^A28^ADT_A05|84951|T|2.4|||||||||||\r"
			+ "EVN|A28|20220705155003||REG_UPDATE|000001^LastName^SARAH^J^^^^^GHS^^^^^LAC||\r"
			+ "PID|1|     Z2883|000008437113^^^MR^MR||ZZZTEST^MENA||20080705|F|||^^^^^USA^H||||||||123-55-1234|||||||||||N||\r"
			+ "ZPD||MYCH|||||N|||||||||||N||||||F|||||||||\r" + "PD1|||GL LA CROSSE^^101010|||||||||||||||\r"
			+ "PV1|1||||||||||||||||||||||||||||||||||||||||||||||||||^^^|||||||||||\r" + "";

	private static final String messageM = "MSH|^~\\&|AMR15|CO|LIS18|KP|20220707192328|M999999|ORM^O01|58770141|P|2.3|||||||||||\r"
			+ "PID|1||001111111^^^KP1043^MR||NAME1-NAME2^NAME3||19700101|F|NAME4^NAME3^^~NAME4-NAME2^NAME3^^~NAME4NAME2^NAME4^^~NAME5^NAME3^^~NAME4-NAME2^NAME3^^|280|1234 AVENUE^^DENVER^CO^80249^USA^RS||(123)456-7890^P^7^^^123^12345678~^NET^Internet^name@gmail.com|(234)567-8901^P^8^^^345^6789012||M||222222222||||AMERICAN/UNI||||||||N||\r"
			+ "PV1||O|1000000000^11111^1976908^222222^^^^^^^|||||||EM||||||||||||||||||||||||||||||||||||||||\r"
			+ "ORC|NW|305209082^KP1125||222222222|||^^^20220707193000^^A^^||20220707192323|M999999^NAME6^NAME7^M^||015228^NAME6^NAME7^MIDDLE (PA)^^^^^KP1045^^^^PRN|1000000000^^^222222^^^^^MED OFFICES|(333)333-3333^^^^^444^4444444||||RMACPAM6687865^RMACPAM6687865^^1000020047^URGENT CARE ACP||||||Medical Offices^12345 Ave^AURORA^CO^80012^USA^C|||||I||||\r"
			+ "OBR|1|305209082^KP1125||COMMP^COMPREHENSIVE METABOLIC PANEL (NA,K,CL,CO2,BUN,CR,GLUC,CA,ALB,TBILI,TPROT,ALT,AST,ALKP)^99153^^Comp meta~87880G^STREPTOCOCCUS GROUP A ANTIGEN, RAPID TEST, POCT^99KP1377^^STREPTOCOCCUS GROUP A ANTIGEN, RAPID TEST, POCT|||||||L||||THRTSW&THROAT SWAB|015228^NAME^TLASTN^FIRSTN (PA)^^^^^KP1045^^^^PRN|(321)456-6789^^^^^345^6789012||||||||||^^^20220707193000^^A^^|||||||||20220707193000|||||||||\r"
			+ "DG1|1|ICD-10-CM|J02.9^ACUTE PHARYNGITIS^ICD-10-CM|ACUTE PHARYNGITIS|\r"
			+ "DG1|2|ICD-10-CM|J06.9^URI (UPPER RESPIRATORY INFECTION)^ICD-10-CM|URI (UPPER RESPIRATORY INFECTION)|\r"
			+ "";

	private static final String messageP = "MSH|^~\\&|EPIC|EPICADT|SMS|SMSADT|199912271408|CHARRIS|ADT^A04|1817457|D|2.5|||||||||||\r"
			+ "PID||0493575^^^2^ID 1|454721||DOE^JOHN^^^^|DOE^JOHN^^^^|19480203|M||B|254 MYSTREET AVE^^MYTOWN^OH^44123^USA||(123)6666666|||M|NON|400003403~1129086|\r"
			+ "NK1||ROE^MARIE^^^^|SPO||(216)123-4567||EC|||||||||||||||||||||||||||\r"
			+ "PV1||O|168 ~219~C~PMA^^^^^^^^^||||277^ALLEN MYLASTNAME^BONNIE^^^^|||||||||| ||2688684|||||||||||||||||||||||||199912271408||||||002376853\r"
			+ "";

	private static final String messageO = "MSH|^~\\&|LIS15|Kaiser Regional Laboratory^05D0698400^CLIA|AMR00|SCA|20220415021946||ORU^R01|Q6819037012T9285912325|P|2.3.1\r"
			+ "PID|1||000022000000^^^KP1013^MR||NAME^NAME^X||20200101|M||Hispanic/Lat|123 STREET ST^^PASADENA^CA^91101^^H^^LOS ANGELE|LOS ANGELE|(123)6666666|||||220000000000||||AMERICAN/UNI\\r"
			+ "PV1|1||BE2-PED3^^^BEL^^^80108||||^^^^^^^^KP1003^^^^PRN\\r"
			+ "ORC|RE|1865020260^KP1119|000022022104040518^KP1020||||||20220415021945|||2056625^CARRION-JACKSON^BIANCA^VALERIA^^^M.D.^^KP1003^^^^PRN||(123)6666666\\r"
			+ "OBR|1|1865020260^KP1119|000022022104040518^KP1020|8354001^Fe TIBC S GL^99139|||20220414122656||||||||BL&Blood|2000000^FIRST-SECOND^FIRST^SECOND^^^M.D.^^KP1003^^^^PRN||||1234567890||20220415021945|||P||1^^^20220414115000^^RO|||111111111\\r"
			+ "NTE|1|ID|RMS ACCN: 720045234rOBX|1|NM|IRON0001^Iron^99266||26|mcg/dL^mcg/dL|43-184^43^184|L|||F|||20220415021945|973A^^99KP1062\\r"
			+ "";

	public TestMLLPClientTLS2way(int port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestMLLPClientTLS2way");
		long start = System.currentTimeMillis();
		HapiContext context = new DefaultHapiContext();

		// Map<String, Object> connProps = new HashMap<String,Object>();

		// PublicKeyStore serverPublicSSLKeyStore = new
		// MockPublicKeyStore("src/test/resources/ClientPublicCert.cer");
		// PublicKeyStore serverPublicSSLKeyStore = new
		// MockPublicKeyStore("src/test/resources/ServerCert2way.cer");
		// connProps.put("partnerPublicSSLCertificate", serverPublicSSLCertificate);
//	        PrivateKeyStore privateSSLKey = new MockPrivateKeyStore("src/test/resources/privateKeyTest123musiq.pfx", "123musiq");
		// PrivateKeyStore privateSSLKey = new
		// MockPrivateKeyStore("src/test/resources/PrivateKeyNew(123musiq).pfx",
		// "123musiq");
		// PrivateKeyStore privateSSLKey = new
		// MockPrivateKeyStore("src/test/resources/client2way.pfx", "client2way");

		// connProps.put("serverPrivateSSLKey", privateSSLCert);

//	        PrivateKeyStore privateSSLKey = new MockPrivateKeyStore("src/test/resources/client2way.pfx", "client2way");
		PrivateKeyStore privateSSLKey = new MockPrivateKeyStore("src/test/resources/eisdev.kp.org.pfx", "xGw]F<9W>jAR");

//	        PublicKeyStore serverPublicSSLKeyStore = new MockPublicKeyStore("src/test/resources/client2way.cer");
		PublicKeyStore serverPublicSSLKeyStore = new MockPublicKeyStore("src/test/resources/eisdev.kp.org303C.cer");

		KeyStore serverkeyStore = privateSSLKey.getKeyStore();
		KeyStore trustKeyStore = serverPublicSSLKeyStore.getKeyStore();
		String clientPrivateSSLKeyPassword = privateSSLKey.getPassword();

		try {
			KeyManagerFactory kmf;
			kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(serverkeyStore, clientPrivateSSLKeyPassword.toCharArray());

			TrustManagerFactory tmf;
			tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustKeyStore);

			CustomCertificateTlsSocketFactoryTLS12Client tlsFac = new CustomCertificateTlsSocketFactoryTLS12Client(kmf,
					tmf, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384");

			// tlsFac.createClientSocket();
			context.setSocketFactory(new HapiSocketTlsFactoryWrapper(tlsFac));

//	               SSLEngine engine =
//	                       SecureSocketSslContextFactory.getClientContext().createSSLEngine();
//	                   engine.setUseClientMode(true);
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Connection connection = null;

		try {
			connection = context.newClient("192.168.0.102", _portNum, true);

		} catch (HL7Exception e1) {
			e1.printStackTrace();
		}
		Initiator initiator = connection.getInitiator();

		// send the created HL7 message over the connection established
		System.out.println("Sending message:" + "\r" + message);
		context.setValidationContext(ValidationContextFactory.noValidation());
		Parser parser = context.getPipeParser();

		Message response = null;
		try {
			response = initiator.sendAndReceive(parser.parse(message));
			// response = initiator.sendAndReceive(messageN);
		} catch (HL7Exception e) {
			e.printStackTrace();
		} catch (LLPException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// display the message response received from the remote party
		String responseString = null;
		try {
			responseString = parser.encode(response);
		} catch (HL7Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Received response:\r" + responseString.replaceAll("\r", "\r"));

		// assertEquals("Hello sync response from your listener. Here is your input
		// echoed back: HELLO from JUNIT\r", new
		// String(actual.get(0).getPayloads().get(0)));
	}

	static String inputStreamToString(InputStream is) throws IOException {
		try (Scanner scanner = new Scanner(is, "UTF-8")) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}

class TestStartListenerNonHL7 implements Runnable {

	static Listener _listener;
	private long _portNum;
	private long _fileSize = 10;

	static final long _threadPoolSize = 1;
	static MLLPUnmanagedListenOperation _operation;

	public TestStartListenerNonHL7(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		_listener = new MockListener();

		MLLPConnector connector = new MLLPConnector();
		ConnectorTester tester = new ConnectorTester(connector);

		Map<String, Object> connProps = new HashMap<String, Object>();

		connProps.put("maximumConnections", _threadPoolSize);
		connProps.put("portNumber", (Long) _portNum);
		connProps.put("nonHL7", true);
		connProps.put("Whitelisting", true);
		connProps.put("doValidation", false);
		connProps.put("useSecureConnection", false);
		connProps.put("selectCipherSuite", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384");
		connProps.put("setCipherSuite", "NONE");
		connProps.put("Address", "192.168.0.104");
		connProps.put("corePoolSize", "10");
		connProps.put("maximumPoolSize", "100");
		connProps.put("keepAliveTime", "30");
		connProps.put("workQueueSize", "100");
		connProps.put("Toggle_IP_DNS", "IP");
		connProps.put("messageSize", (Long) _fileSize);
		connProps.put("nonHL7MLLPWrapper", "Yes");
		connProps.put("mllpAck", "Yes");
		Map<String, Object> opProps = new HashMap<String, Object>();

		tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
		_operation = new MLLPUnmanagedListenOperation(tester.getOperationContext());
		_operation.start(_listener);
	}
}

class TestMLLPnonHL7Client extends Thread {

	private long _portNum;

	public TestMLLPnonHL7Client(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		long start = System.currentTimeMillis();
		int __portNum = (int) _portNum;
		System.out.println("Client started");
		try {
			try (// Socket socket = new Socket("192.168.0.102", __portNum);

					Socket socket = new Socket("192.168.0.108", __portNum)) {
				OutputStream output = socket.getOutputStream();

				// socket;

				String payload4 = "";
				String payload3 = "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\r"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "ASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
						+ "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
						+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG";
				String payload2 = "<abc></abc>";
				
				String payloadM = "MSH|^~\\&|LIS15|Kaiser Regional Laboratory^05D0698400^CLIA|AMR00|SCA|202\r"
						+ "20415021946||ORU^R01|Q6819037012T92859123AAS|P|2.3.1\r"
						+ "PPID|1||000022000000^^^KP1013^MR||NAME^NAME^X||20200101|M||Hispanic/Lat|123 STREET ST^^PASADENA^CA^91101^^H^^LOS ANGELE|LOS ANGELE|(123)6666666|||||220000000000||||AMERICAN/UNI\\r"
						+ "PV1|1||BE2-PED3^^^BEL^^^80108||||^^^^^^^^KP1003^^^^PRN\\r"
						+ "ORC|RE|1865020260^KP1119|000022022104040518^KP1020||||||20220415021945|||2056625^CARRION-JACKSON^BIANCA^VALERIA^^^M.D.^^KP1003^^^^PRN||(123)6666666\\r"
						+ "OBR|1|1865020260^KP1119|000022022104040518^KP1020|8354001^Fe TIBC S GL^99139|||20220414122656||||||||BL&Blood|2000000^FIRST-SECOND^FIRST^SECOND^^^M.D.^^KP1003^^^^PRN||||1234567890||20220415021945|||P||1^^^20220414115000^^RO|||111111111\\r"
						+ "NTE|1|ID|RMS ACCN: 720045234rOBX|1|NM|IRON0001^Iron^99266||26|mcg/dL^mcg/dL|43-184^43^184|L|||F|||20220415021945|973A^^99KP1062\\r"
						+ "";
				String payload = "MSH+^~\\&+KPDS+SCA.BEL+CBORD+SCA+20220406021608-0700++ADT^A08^ADT_A01+0910094786+T+2.3.1+++NE+NE\r"
						+ "EVN++AS22040602160800-0700++REG_UPDATE+K000000+2022040602161-0700\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PID+1++000011111111^^^KP1013^MR~888888888^^^USSSA^SS++LAST^FIRST++19670101+F+LAST^FIRST+O^Other^HL70005+++^NET^Internet^name@gmail.com+++++220000000000++++N^NONHISPANIC^HL70189++++++USA++N\r"
						+ "PV1+1+E+BDMED^BBG62^62^BEL^D^^80101^^DMED+ER+++7560796^LEE^DAMIEN^BENNETT (M.D.)^^^^^KP1003^^^^PRN+++EM++++++++000+220405148663^^^KP1066^^BEL+113++++++++++++++++++++++++20220405212600";

			//	System.out.println("length " + payload.getBytes().length);
				
				
				String finalReturnMessage = payload3;
				byte[] bytes = finalReturnMessage.getBytes(StandardCharsets.US_ASCII);
				StringBuilder str = new StringBuilder();
				List<Integer> result = new ArrayList<>(); // convert bytes to ascii
				int ascii;
				result.add(11);
				for (byte aByte : bytes) {
					ascii = (int) aByte; // byte -> int
					result.add(ascii);
				}
				result.add(28);
				result.add(13);

				int[] asciiArray = ArrayUtils.toPrimitive(result.toArray(new Integer[0]));

		//		System.out.println(Arrays.toString(asciiArray));
				for (int i : asciiArray) {
					str.append(Character.toString((char) i));
				}
				
				System.out.println(str.toString());
				byte[] data = str.toString().getBytes();
				output.write(data);

				output.flush();

				System.out.println("Open socket input stream to get response");

				BufferedReader socketIn = new BufferedReader(
						new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
				StringBuilder socketResponse = new StringBuilder();
//	socketResponse.append();
				int c;
				while ((c = socketIn.read()) != -1) {

					socketResponse.append((char) c);
					if (!socketIn.ready())
						break;
				}

				System.out.println("clientSocket.isClosed()" + socket.isClosed());

				System.out.println(String.format("Response from socket server") + (socketResponse.toString()));

				socketIn.close();
				output.close();

//	in.close();
//	socket.close();
			}

			// PrintWriter writer = new PrintWriter(output, true);
			// writer.println(payload);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Total Time" + (System.currentTimeMillis() - start));
		// assertEquals("Hello sync response from your listener. Here is your input
		// echoed back: HELLO from JUNIT\r", new
		// String(actual.get(0).getPayloads().get(0)));

	}
}

class TestStartListenerNonHL7TLS implements Runnable {

	static Listener _listener;
	private long _portNum;
	private long _fileSize = 10;

	static final long _threadPoolSize = 1;
	static MLLPUnmanagedListenOperation _operation;

	public TestStartListenerNonHL7TLS(long port) {
		_portNum = port;
	}

	@Override
	public void run() {

		_listener = new MockListener();

		MLLPConnector connector = new MLLPConnector();
		ConnectorTester tester = new ConnectorTester(connector);

		Map<String, Object> connProps = new HashMap<String, Object>();

		connProps.put("maximumConnections", _threadPoolSize);
		connProps.put("portNumber", (Long) _portNum);
		connProps.put("nonHL7", true);
		connProps.put("Whitelisting", true);
		connProps.put("doValidation", false);
		connProps.put("useSecureConnection", true);
		connProps.put("selectCipherSuite", "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384");
		connProps.put("setCipherSuite", "NONE");
		// connProps.put("Address", "192.168.0.104");
		connProps.put("corePoolSize", "10");
		connProps.put("maximumPoolSize", "100");
		connProps.put("keepAliveTime", "30");
		connProps.put("workQueueSize", "100");
		connProps.put("Toggle_IP_DNS", "IP");
		connProps.put("messageSize", (Long) _fileSize);
		connProps.put("nonHL7MLLPWrapper", "No");
		Map<String, Object> opProps = new HashMap<String, Object>();

		PublicKeyStore partnerPublicSSLCertificate = new MockPublicKeyStore("src/test/resources/CAcert.cer");
		// PrivateKeyStore serverPrivateSSLKey = new
		// MockPrivateKeyStore("src/test/resources/ServerCert2way.pfx", "server2way");
		PrivateKeyStore serverPrivateSSLKey = new MockPrivateKeyStore("src/test/resources/eisdev.kp.org.pfx",
				"xGw]F<9W>jAR");
		connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);
		connProps.put("serverPrivateSSLKey", serverPrivateSSLKey);

		tester.setOperationContext(OperationType.LISTEN, connProps, opProps, null, null);
		_operation = new MLLPUnmanagedListenOperation(tester.getOperationContext());
		_operation.start(_listener);
	}
}

class TestMLLPnonHL7TLSClient extends Thread {

	private long _portNum = 9907;

	public TestMLLPnonHL7TLSClient(long port) {
		_portNum = port;
	}

	@Override
	public void start() {

		long start = System.currentTimeMillis();
		int __portNum = (int) _portNum;

//	        PrivateKeyStore privateSSLKey = new MockPrivateKeyStore("src/test/resources/client2way.pfx", "client2way");
		PrivateKeyStore privateSSLKey = new MockPrivateKeyStore("src/test/resources/eisdev.kp.org.pfx", "xGw]F<9W>jAR");

//	        PublicKeyStore serverPublicSSLKeyStore = new MockPublicKeyStore("src/test/resources/derEncoded.cer");
		PublicKeyStore serverPublicSSLKeyStore = new MockPublicKeyStore("src/test/resources/eisdev.kp.org303C.cer");

		try {
			SSLSocketFactory factory = null;

			SSLContext ctx;
			TrustManagerFactory tmf;
			KeyManagerFactory kmf;
			KeyStore ks;
			char[] passphrase = "client2way".toCharArray();

			ctx = SSLContext.getInstance("TLSv1.2");
			tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			ks = serverPublicSSLKeyStore.getKeyStore();
			KeyStore privateSSLKeyStore = privateSSLKey.getKeyStore();
			String clientPrivateSSLKeyPassword = privateSSLKey.getPassword();
			kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			try {
				kmf.init(privateSSLKeyStore, clientPrivateSSLKeyPassword.toCharArray());
			} catch (UnrecoverableKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			tmf.init(ks);
			// TrustManager[] trustManagers = tmf.getTrustManagers();
			ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());

			factory = ctx.getSocketFactory();

			SSLSocket socket = (SSLSocket) factory.createSocket("localhost", __portNum);

			String payload = "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\r"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "ASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS"
					+ "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "HELLO from BOOMI AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAG";

			System.out.println("length " + payload.getBytes().length);
			byte[] data = payload.getBytes();

			socket.startHandshake();

			System.out.println("SSL handshake");
			OutputStream output = socket.getOutputStream();
			output.write(data);

			output.flush();

			System.out.println("Open socket input stream to get response");

			BufferedReader socketIn = new BufferedReader(
					new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
			StringBuilder socketResponse = new StringBuilder();
//	socketResponse.append();
			int c;
			while ((c = socketIn.read()) != -1) {

				socketResponse.append((char) c);
				if (!socketIn.ready())
					break;
			}

			System.out.println("clientSocket.isClosed()" + socket.isClosed());

			System.out.println(String.format("Response from socket server") + (socketResponse.toString()));

			socketIn.close();
			output.close();

//	in.close();
//	socket.close();

			// PrintWriter writer = new PrintWriter(output, true);
			// writer.println(payload);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Total Time" + (System.currentTimeMillis() - start));
		// assertEquals("Hello sync response from your listener. Here is your input
		// echoed back: HELLO from JUNIT\r", new
		// String(actual.get(0).getPayloads().get(0)));

	}
}

class TestMLLPClientTLS1wayLoad extends Thread {

	private int _portNum;

	private static final String message1 = "MSH|^~\\&|AMR15|CO|LIS18|KP|20220707192328|M999999|ORM^O01|58770141|P|2.3|||||||||||\r"
			+ "PID|1||001111111^^^KP1043^MR||NAME1-NAME2^NAME3||19700101|F|NAME4^NAME3^^~NAME4-NAME2^NAME3^^~NAME4NAME2^NAME4^^~NAME5^NAME3^^~NAME4-NAME2^NAME3^^|280|1234 AVENUE^^DENVER^CO^80249^USA^RS||(123)456-7890^P^7^^^123^12345678~^NET^Internet^name@gmail.com|(234)567-8901^P^8^^^345^6789012||M||222222222||||AMERICAN/UNI||||||||N||\r"
			+ "PV1||O|1000000000^11111^1976908^222222^^^^^^^|||||||EM||||||||||||||||||||||||||||||||||||||||\r"
			+ "ORC|NW|305209082^KP1125||222222222|||^^^20220707193000^^A^^||20220707192323|M999999^NAME6^NAME7^M^||015228^NAME6^NAME7^MIDDLE (PA)^^^^^KP1045^^^^PRN|1000000000^^^222222^^^^^MED OFFICES|(333)333-3333^^^^^444^4444444||||RMACPAM6687865^RMACPAM6687865^^1000020047^URGENT CARE ACP||||||Medical Offices^12345 Ave^AURORA^CO^80012^USA^C|||||I||||\r"
			+ "OBR|1|305209082^KP1125||COMMP^COMPREHENSIVE METABOLIC PANEL (NA,K,CL,CO2,BUN,CR,GLUC,CA,ALB,TBILI,TPROT,ALT,AST,ALKP)^99153^^Comp meta~87880G^STREPTOCOCCUS GROUP A ANTIGEN, RAPID TEST, POCT^99KP1377^^STREPTOCOCCUS GROUP A ANTIGEN, RAPID TEST, POCT|||||||L||||THRTSW&THROAT SWAB|015228^NAME^TLASTN^FIRSTN (PA)^^^^^KP1045^^^^PRN|(321)456-6789^^^^^345^6789012||||||||||^^^20220707193000^^A^^|||||||||20220707193000|||||||||\r"
			+ "DG1|1|ICD-10-CM|J02.9^ACUTE PHARYNGITIS^ICD-10-CM|ACUTE PHARYNGITIS|\r"
			+ "DG1|2|ICD-10-CM|J06.9^URI (UPPER RESPIRATORY INFECTION)^ICD-10-CM|URI (UPPER RESPIRATORY INFECTION)|\r"
			+ "";
	private static final String message3 = "MSH|^~\\&|GHH LAB|ELAB-3|GHH OE|BLDG4|200202150930||ORU^R01|CNTRL-3460|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";
	private static final String message2 = "MSH|^~\\&|GHH LAB|ELAB-3|GHH123 OE|BLDG4|200202150930||ORU^R01|CNTRL-3460|P|2.5|||||\r"
			+ "PID|||555-44-4444||EVERYWOMAN^EVE^E^^^^L|JONES|19620320|F|||153 FERNWOOD DR.^^STATESVILLE^OH^35292||(206)3345232|(206)752-121||||AC555444444||67-A4335^OH^20030520\r"
			+ "OBR|1|845439^GHH OE|1045813^GHH LAB|15545^GLUCOSE|||200202150730|||||||||555-55-5555^PRIMARY^PATRICIA P^^^^MD^^|||||||||F||||||444-44-4444^HIPPOCRATES^HOWARD H^^^^MD\r"
			+ "OBX|1|SN|1554-5^GLUCOSE^POST 12H CFST:MCNC:PT:SER/PLAS:QN||^182|mg/dl|70_105|H|||F\r" + "";

	public TestMLLPClientTLS1wayLoad(int port) {
		_portNum = port;
	}

	@Override
	public void run() {

		System.out.println("run TestMLLPClientTLS1way");
		long start = System.currentTimeMillis();
		HapiContext context1 = new DefaultHapiContext();
		HapiContext context2 = new DefaultHapiContext();
		context1.getParserConfiguration().setValidating(false);
		context2.getParserConfiguration().setValidating(false);

		Map<String, Object> connProps = new HashMap<String, Object>();

//	       PublicKeyStore partnerPublicSSLCertificate = new MockPublicKeyStore("src/test/resources/ServerPublicCert.cer");
		// PublicKeyStore partnerPublicSSLCertificate = new
		// MockPublicKeyStore("src/test/resources/ClientPublicCert.cer");

		PublicKeyStore partnerPublicSSLCertificate = new MockPublicKeyStore("src/test/resources/BoomiSSLPK.cer");

		connProps.put("partnerPublicSSLCertificate", partnerPublicSSLCertificate);

		KeyStore keyStore = null;
		try {
			keyStore = partnerPublicSSLCertificate.getKeyStore();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			KeyStore.getInstance("PKCS12");
		} catch (KeyStoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			KeystoreUtils.validateKeystoreForTlsSending(keyStore);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CustomCertificateTlsSocketFactory tlsFac = new CustomCertificateTlsSocketFactory(keyStore, "");
		context1.setSocketFactory(new HapiSocketTlsFactoryWrapper(tlsFac));
		context2.setSocketFactory(new HapiSocketTlsFactoryWrapper(tlsFac));

		// create a new MLLP client over the specified port
		Connection connection1 = null;
		// Connection connection2 = null;

		for (int i = 0; i < 1000; i++) {

			try {
				// new TestMLLPnonHL7Client(_portNumNonHL7).run();
				System.out.println("Creation New Client connection");
				connection1 = context1.newClient("ExtLBFormMolecule-1529742430.us-east-2.elb.amazonaws.com", _portNum,
						true);
				// connection2 = context2.newClient("192.168.0.102", _portNum, true);
			} catch (HL7Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// System.out.println("Connection");
			// System.out.println(connection.toString());

			// The initiator which will be used to transmit our message
			Initiator initiator1 = connection1.getInitiator();
			// Initiator initiator2 = connection2.getInitiator();

			// send the created HL7 message over the connection established
			Parser parser1 = context1.getPipeParser();
			// Parser parser2 = context2.getPipeParser();
			// System.out.println("Sending message:" + "\r" + message1);
			Message response1 = null;
			// Message response2 = null;
			// String message1 = message005 + i + message05;

			try {
				response1 = initiator1.sendAndReceive(parser1.parse(message1));
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LLPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Initiator initiator = connection1.getInitiator();

			String responseString1 = null;

			ACK ackMessage = null;

			// ACK_A01 ackMessage = null;

			// try {
			// ackMessage = (ACK) AdtMessageFactory.createMessage("AR");
			// } catch (HL7Exception e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			// } catch (IOException e1) {
			// TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			System.out.println("Sending message:" + "\r" + ackMessage.toString());
			try {
				response1 = initiator1.sendAndReceive(ackMessage);
				// response2 = initiator.sendAndReceive(parser.parse(message));
			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LLPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				responseString1 = parser1.encode(response1);

			} catch (HL7Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("Received response 1 :\r" + responseString1.replaceAll("\r", "\r"));
			// System.out.println("Received response 2 :\r" +
			// responseString2.replaceAll("\r", "\r"));

			System.out.println("Total Time" + (System.currentTimeMillis() - start));
			// assertEquals("Hello sync response from your listener. Here is your input
			// echoed back: HELLO from JUNIT\r", new
			// String(actual.get(0).getPayloads().get(0)));
		}

	}

	static String inputStreamToString(InputStream is) throws IOException {
		try (Scanner scanner = new Scanner(is, "UTF-8")) {
			return scanner.useDelimiter("\\A").next();
		}
	}
}