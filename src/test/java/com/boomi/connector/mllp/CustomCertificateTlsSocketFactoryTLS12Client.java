package com.boomi.connector.mllp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.util.logging.Logger;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import ca.uhn.hl7v2.hoh.sockets.ISocketFactory;

public class CustomCertificateTlsSocketFactoryTLS12Client implements ISocketFactory  {

	Logger logger = Logger.getLogger(CustomCertificateTlsSocketFactoryTLS12Client.class.getName());

	private TrustManagerFactory tmf;
//	private SSLServerSocketFactory myServerSocketFactory;
	private String myCipherSuite;
	private KeyManagerFactory kmf;

	private SSLSocketFactory mySocketFactory = null;

	/**
	 * Constructor
	 */
	public CustomCertificateTlsSocketFactoryTLS12Client() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @throws NullPointerException
	 *             If theKeystore is null
	 */
	public CustomCertificateTlsSocketFactoryTLS12Client(KeyManagerFactory _kmf, TrustManagerFactory _trustManagerFactory, String _CipherSuite) {
		
		kmf = _kmf;
	//	myKeystorePassphrase = theKeystorePass;
		myCipherSuite = _CipherSuite;
		tmf = _trustManagerFactory;
//		port = _port;
	}

	/**
	 * {@inheritDoc}
	 */
	
	@Override
	public Socket createClientSocket() throws IOException {
		// TODO Auto-generated method stub
		initialize();
		System.out.println("Creating client socket");
		return mySocketFactory.createSocket();
	}
	
	private void initialize() throws IOException {
		if (mySocketFactory != null) {
			return;
		}

			try {
			SSLContext ctx = SSLContext.getInstance("TLSv1.2");
			
			TrustManager[] trustManagers = tmf.getTrustManagers();
			KeyManager[] keyManagers = kmf.getKeyManagers();
				
				ctx.init(keyManagers, trustManagers, null);
				SSLEngine engine = ctx.createSSLEngine();		
	    					     
			     engine.setUseClientMode(true);
			     engine.beginHandshake();
		
			     mySocketFactory = ctx.getSocketFactory();
				
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
finally
{
	System.out.println("SSL Context set");
}
	}

	@Override
	public ServerSocket createServerSocket() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
