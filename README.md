# Welcome to the Boomi MLLP Connector Open Source Project.

This connector provides a HL7 MLLP Server/listener.

This connector is built on Netty and performs 2 basic functions:
1) Allow HL7 content over MLLP and validate the HL7 basic structure ans generate acknowledgements in HL7
2) Allow Non HL7 content over MLLP, and provides the option to acknowledge in customized text : example OK on successful processing and ERROR when unable to process.

To install the connector to your account, please follow the following instructions.

https://help.boomi.com/bundle/connectors/page/t-atm-Adding_a_connector_group.html

The connector-descriptor.xml and MLLPConnector*.zip files are at the root of this project.
